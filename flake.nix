{
  description = "Lydia (Twig)'s NixOS Configuration :3";

  nixConfig = {
    extra-substituters = [
      "https://hyprland.cachix.org"
      "https://cache.nixos.org/"
      "https://nix-community.cachix.org"
      "https://niri.cachix.org"
    ];
    extra-trusted-public-keys = [
      "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "niri.cachix.org-1:Wv0OmO7PsuocRKzfDoJ3mulSl7Z6oezYhGhR+3W2964="
    ];
  };

  outputs = {
    sops-nix,
    hardware,
    nixpkgs,
    home-manager,
    priv,
    ...
  } @ inputs: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
    req-modules = [
      priv.nixosModules.secret
      sops-nix.nixosModules.sops
      ./common
    ];
    home-module = [
      home-manager.nixosModules.home-manager
      {
        home-manager = {
          extraSpecialArgs = {inherit inputs;};
          useGlobalPkgs = true;
          useUserPackages = true;
        };
      }
    ];
  in {
    formatter.${system} = pkgs.alejandra;

    devShells.${system}.default = pkgs.mkShell {
      packages = [
        pkgs.alejandra
      ];
    };

    nixosConfigurations = {
      lotus-solus = nixpkgs.lib.nixosSystem {
        specialArgs = {inherit inputs;};
        modules =
          req-modules
          ++ home-module
          ++ [
            ./hosts/lotus-solus
          ];
      };
      optiplex3020-nixos = nixpkgs.lib.nixosSystem {
        specialArgs = {inherit inputs;};
        modules =
          req-modules
          ++ home-module
          ++ [
            ./hosts/optiplex3020-nixos
          ];
      };
    };

    packages.${system} = {
      twig-shell = inputs.astal.lib.mkLuaPackage {
        inherit pkgs;
        name = "twig-shell"; # how to name the executable
        src = ./common/optional/users/ags/src; # should contain init.lua

        # add extra glib packages or binaries
        extraPackages = [
          inputs.astal.packages.${system}.battery
          pkgs.dart-sass
        ];
      };
      soft-serve = pkgs.callPackage ./pkgs/soft-serve.nix {};
    };
  };

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-wayland.url = "github:nix-community/nixpkgs-wayland";
    hardware.url = "github:NixOS/nixos-hardware/master";

    niri = {
      url = "github:sodiboo/niri-flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    umu-launcher = {
      url = "github:Open-Wine-Components/umu-launcher?dir=packaging/nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-minecraft.url = "github:Infinidoge/nix-minecraft";

    spicetify-nix = {
      url = "github:Gerg-L/spicetify-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    quickshell = {
      url = "git+https://git.outfoxxed.me/outfoxxed/quickshell";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    astal = {
      url = "github:aylur/astal";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
    };

    nvf = {
      url = "github:notashelf/nvf";
    };

    nvim-nightly = {
      url = "github:nix-community/neovim-nightly-overlay";
    };

    schizofox = {
      url = "github:schizofox/schizofox";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # hyprland.url = "github:hyprwm/Hyprland";
    # hyprgrass = {
    # url = "github:horriblename/hyprgrass";
    # inputs.hyprland.follows = "hyprland";
    # };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    priv = {
      url = "git+file:///home/ly/nixos/priv/";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
}
