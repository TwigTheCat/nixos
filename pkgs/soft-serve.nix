{
  lib,
  buildGoModule,
  fetchFromGitHub,
  makeWrapper,
  nixosTests,
  git,
  bash,
}:
buildGoModule rec {
  pname = "soft-serve";
  version = "0.8.2-unstable-2025-2-16";

  src = fetchFromGitHub {
    owner = "charmbracelet";
    repo = "soft-serve";
    rev = "025edaa354438ed9a4777d84a61f7834a3b5d10d";
    hash = "sha256-O1S6reeb+mFjNbiiT4sEToGghHIS0pYB4frd7KTE2e8=";
  };

  vendorHash = "sha256-F4Q3PjuIZMLufPRxCkWCmnBq6E+ajT2VX2zUqiBLQXQ=";

  doCheck = false;

  ldflags = ["-s" "-w" "-X=main.Version=${version}"];

  nativeBuildInputs = [makeWrapper];

  postInstall = ''
    # Soft-serve generates git-hooks at run-time.
    # The scripts require git and bash inside the path.
    wrapProgram $out/bin/soft \
      --prefix PATH : "${lib.makeBinPath [git bash]}"
  '';

  passthru.tests = nixosTests.soft-serve;
}
