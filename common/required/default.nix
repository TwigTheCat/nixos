{lib, ...}: {
  home-manager.sharedModules = [
    ./users
  ];
  imports = [
    ./bootloader.nix
    ./init.nix
    ./local.nix
    ./user.nix
    ./nix.nix
    ./network.nix
  ];
  systemd.user.services.niri-flake-polkit.enable = false;
  services.gnome.gnome-keyring.enable = lib.mkDefault false;
}
