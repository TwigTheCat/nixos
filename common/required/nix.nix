{
  pkgs,
  inputs,
  lib,
  ...
}: {
  nixpkgs = {
    config.allowUnfree = true;
    overlays = [
      inputs.emacs-overlay.overlay
      inputs.niri.overlays.niri
    ];
  };
  nix = {
    package = pkgs.lix;
    settings = {
      trusted-users = ["@wheel" "root"];
      experimental-features = ["nix-command" "flakes"];
      auto-optimise-store = true;
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 4d";
    };
  };
  system.rebuild.enableNg = true;

  # command-not-found doesn't work with flakes due to not being able to have a sqlite db
  programs.command-not-found.enable = false;

  environment.systemPackages = [
    (pkgs.writeShellScriptBin "nom-switch" ''
      CWD=$(pwd)
      cd /tmp/

      if nixos-rebuild build --flake $CWD --log-format internal-json -v |& ${lib.getExe pkgs.nix-output-monitor} --json; then
        ${lib.getExe pkgs.nvd} diff /run/current-system/ result
        nixos-rebuild switch --quiet --flake $CWD --sudo
        rm -rf result
      fi
      cd $CWD
    '')
  ];
}
