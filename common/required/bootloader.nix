{...}: {
  boot = {
    loader = {
      # you would not catch me dead using grub in any of my installs
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    kernelParams = [
      "quiet"
      "splash"
      "fbcon=nodefer" # less vendor logo
      "systemd.show_status=auto" # systemd errors only
      "udev.log_level=3" # udev errors only
    ];
    consoleLogLevel = 3;
  };
}
