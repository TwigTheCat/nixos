{...}: {
  networking = {
    dhcpcd.enable = false;
    enableIPv6 = true;
    useDHCP = false;
    useNetworkd = true;
  };
  systemd = {
    network.enable = true;
    network.wait-online.enable = false;
  };
  services = {
    resolved = {
      enable = true;
      dnsovertls = "opportunistic";
      dnssec = "false";
      fallbackDns = [
        "9.9.9.9"
        "127.0.0.1"
      ];
      extraConfig = "Domains=~.";
    };

    avahi = {
      enable = true;
      nssmdns4 = true;
      nssmdns6 = true;
      publish = {
        enable = true;
        addresses = true;
        domain = true;
        hinfo = true;
        userServices = true;
        workstation = true;
      };
    };
  };
}
