{pkgs, ...}: {
  twig = {
    theming.enable = true;
    theming.fonts.enable = true;
    programs = {
      cli.enable = true;

      schizofox.enable = true;
      hyprland.enable = true;
      hyprlock.enable = true;

      foot.enable = true;
      fuzzel.enable = true;

      nvf.enable = true;

      river.enable = false;
    };

    services = {
      # hypridle.enable = true;
      idle = {
        enable = true;
        idleService = "swayidle";
        locker = pkgs.swaylock;
        lockerArguments = "-i ${../../optional/users/wallpapers/spookys-mansion.png}";
      };
      hyprpaper.enable = true;

      mako.enable = true;
    };
  };

  home.stateVersion = "24.05";
}
