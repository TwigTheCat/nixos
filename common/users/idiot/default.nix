{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.twig.users.idiot;
in {
  options.twig.users.idiot = {
    enable = lib.mkEnableOption "idiot users";
  };

  config = lib.mkIf cfg.enable {
    home-manager.users.idiot = import ./home.nix;

    users.users.idiot = {
      hashedPasswordFile = config.sops.secrets.idiotPass.path;
      isNormalUser = true;
      shell = pkgs.zsh;
      ignoreShellProgramCheck = true;
    };
  };
}
