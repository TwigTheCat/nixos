{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.twig.users.ly;
in {
  options.twig.users.ly = {
    enable = lib.mkEnableOption "ly user";
  };

  config = lib.mkIf cfg.enable {
    home-manager.users.ly = import ./home.nix;

    nixpkgs.config.permittedInsecurePackages = [
      "fluffychat-linux-1.23.0"
      "olm-3.2.16"
    ];

    users.users.ly = {
      hashedPasswordFile = config.sops.secrets.lyPass.path;
      isNormalUser = true;
      shell = pkgs.fish;
      ignoreShellProgramCheck = true;
      extraGroups = ["wheel" "input" "uinput" "video"];
    };
  };
}
