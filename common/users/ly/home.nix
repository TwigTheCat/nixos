{
  pkgs,
  inputs,
  ...
}: let
  dorionNew =
    pkgs.dorion.overrideAttrs
    (
      final: prev: {
        buildInputs = prev.buildInputs ++ [pkgs.webkitgtk_4_1];
        meta.mainProgram = "Dorion";
        version = "6.4.1";
        src = pkgs.fetchurl {
          url = "https://github.com/SpikeHD/Dorion/releases/download/v${final.version}/Dorion_${final.version}_amd64.deb";
          hash = "sha256-fAJi3jX6ReRfkNTpeLeHW6SYw9KKZHUDPp74f067yew=";
        };
      }
    );
in {
  twig = {
    desktop = {
      enable = true;
      terminal = {
        wayland = {
          enable = true;
          package = pkgs.foot.overrideAttrs (final: prev: {
            version = "1.20.1-unstable-2025-2-16";
            src = pkgs.fetchFromGitea {
              domain = "codeberg.org";
              owner = "dnkl";
              repo = "foot";
              rev = "ba5f4abdd47e572f88e4ccba75cd8af82f6ee507";
              hash = "sha256-hnCcsRzNDgyoPWJKd4hoGI0I3ApAfu6DfY3TnjQg8ZA=";
            };
          });
        };
      };
    };
    theming = {
      enable = true;
      fonts.enable = true;
    };
    programs = {
      prism.enable = true;

      gui.enable = true;
      cli.enable = true;
      carapace.enable = true;
      zoxide.zsh.enable = true;
      zoxide.fish.enable = true;

      pay-respects = {
        enable = true;
        fishIntegration = true;
      };

      music.enable = true;
      mpd.enable = false;

      nix-index = {
        enable = true;
        fishIntegration = true;
      };

      schizofox.enable = true;
      librewolf.enable = false;
      ungoogled-chromium.enable = true;

      fish.enable = true;
      nushell = {
        enable = true;
        carapace.enable = true;
      };
      zsh = {
        enable = false;
        carapace.enable = false;
      };

      emacs = {
        enable = true;
        client.enable = true;
        standalone.enable = true;
      };

      helix.enable = false;
      nvf.enable = true;

      hyprland.enable = true;
      hyprlock.enable = false;

      niri.enable = false;

      foot.enable = true;
      fuzzel.enable = false;

      spicetify.enable = true;

      river.enable = true;

      ghostty.enable = false;
    };

    services = {
      hyprpaper.enable = false;

      ags.enable = true;

      idle = {
        enable = true;
        idleService = "swayidle";
        locker = pkgs.swaylock;
        lockerArguments = "-i ${../../optional/users/wallpapers/spookys-mansion.png}";
      };

      wbg = {
        enable = true;
        bg-path = "${../../optional/users/wallpapers/kinger-wiiiiiiifr-abstracted-tumblr-ff.jpg}";
      };

      mako.enable = true;
    };
  };

  #TODO: make modules for each program/groups of programs
  # eg: tui/gui programs
  home.packages = with pkgs; [
    inputs.nixpkgs-wayland.packages.${pkgs.system}.wbg
    wvkbd
    libinput-gestures
    tt
    equibop
    dorionNew

    gnome-boxes

    signal-desktop
    fluffychat

    texlive.combined.scheme-medium

    hunspell
    hunspellDicts.en_US

    most
    wl-clipboard

    wf-recorder

    xfce.thunar

    pamixer
    playerctl
    pwvucontrol

    nil

    wineWowPackages.waylandFull
    winetricks
    protontricks
    lutris
    inputs.umu-launcher.packages.${pkgs.system}.default
    gamescope
    gamemode
  ];

  programs.tofi = {
    enable = true;
    settings = {
      font-size = 11;
      placeholder-text = "meow :3";
      border-width = 3;
      padding-left = "25";
      padding-right = "25";
      padding-top = "15";
      padding-bottom = "15";
      result-spacing = "10";

      drun-launch = true;

      width = "350";
      height = "550";

      background-color = "#0F1010";
      border-color = "#F8C8DC";
      selection-color = "#F39AC2";
      selection-match-color = "#F8C8DC";
    };
  };

  services.easyeffects = {
    enable = true;
  };

  home.stateVersion = "24.05";
}
