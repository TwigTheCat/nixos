{
  config,
  pkgs,
  lib,
  ...
}: let
  inherit (lib) mkEnableOption mkOption mkIf;
  inherit (lib.types) nullOr str;

  snapCfg = config.twig.services.snapcast;
  cfg = config.twig.services.mpd;
in {
  options.twig.services.mpd = {
    enable = mkEnableOption "mpd";
    snapserverOutput = mkEnableOption "snapserver output for mpd";
    musicDir = mkOption {
      type = nullOr str;
      default = null;
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = cfg.snapserverOutput && snapCfg.mpdLocation != null && snapCfg.enable;
        message = ''
          The snapserver output was enabled for mpd, but either snapserver is not enabled, or mpdLocation is set to null.
        '';
      }
    ];
    services.mpd = {
      enable = true;
      musicDirectory = mkIf (cfg.musicDir != null) cfg.musicDir;
      network.listenAddress = "any";
      extraConfig = mkIf cfg.snapserverOutput ''
        audio_output {
            type "fifo"
            name "pipe"
            path "${snapCfg.mpdLocation}"
            format "48000:16:2"
            mixer_type "software"
        }
      '';
    };
  };
}
