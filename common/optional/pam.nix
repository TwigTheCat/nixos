{
  pkgs,
  lib,
  config,
  ...
}: let
  cfg = config.twig.pamU2fIfWheel;
in {
  options.twig.pamU2fIfWheel = {
    enable = lib.mkEnableOption "pam u2f check for wheel users";
  };

  config = lib.mkIf cfg.enable {
    security.pam.u2f.enable = true;
    security.pam.services = let
      u2fIfWheel = {
        unix = {
          enable = true;
          order = 100;
          control = lib.mkForce "sufficient";
          modulePath = "${pkgs.pam.outPath}/lib/security/pam_unix.so";
        };
        u2f = {
          enable = true;
          order = 50;
          control = lib.mkForce "required";
          modulePath = "${pkgs.pam_u2f.outPath}/lib/security/pam_u2f.so";
        };
        wheelCheck = {
          enable = true;
          order = config.security.pam.services.login.rules.auth.u2f.order - 1;
          control = "[default=1 success=ignore]";
          modulePath = "${pkgs.pam.outPath}/lib/security/pam_succeed_if.so";
          args = [
            "user"
            "ingroup"
            "wheel"
          ];
        };
      };
    in {
      login.rules.auth = u2fIfWheel;
      hyprlock.rules.auth = u2fIfWheel;
    };
  };
}
