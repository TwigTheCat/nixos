{
  config,
  pkgs,
  lib,
  ...
}: let
  inherit (lib) mkEnableOption mkOption mkIf;
  inherit (lib.types) nullOr str;

  cfg = config.twig.services.snapcast;
in {
  options.twig.services.snapcast = {
    enable = mkEnableOption "snapserver";
    client = {
      enable = mkEnableOption "snapclient";
      host = mkOption {
        type = str;
        default = "";
        description = ''
          The host address of the snapserver you want the client to stream from.
        '';
      };
      port = mkOption {
        type = str;
        default = "";
        description = ''
          The port of the snapserver you want the client to stream from.
        '';
      };
    };
    mpdLocation = mkOption {
      type = nullOr str;
      default = null;
      description = ''
        The path that the snapserver should listen to for streaming audio from MPD.
      '';
    };
  };

  config = {
    services.snapserver = mkIf cfg.enable {
      enable = true;
      codec = "flac";
      streams = {
        mpd = {
          type = "pipe";
          location = cfg.mpdLocation;
          sampleFormat = "48000:16:2";
          codec = "flac";
        };
      };
    };

    systemd.user.services.snapcast-sink = mkIf (cfg.enable
      && cfg.mpdLocation != null) {
      wantedBy = [
        "pipewire.service"
      ];
      after = [
        "pipewire.service"
      ];
      bindsTo = [
        "pipewire.service"
      ];
      path = [
        pkgs.gawk
        pkgs.pulseaudio
      ];
      script = ''
        pactl load-module module-pipe-sink file=${cfg.mpdLocation} sink_name=Snapcast format=s16le rate=48000
      '';
    };

    systemd.user.services.snapclient-local = mkIf cfg.client.enable {
      wantedBy = [
        "pipewire.service"
      ];
      after = [
        "pipewire.service"
      ];
      serviceConfig = {
        ExecStart = "${lib.getExe' pkgs.snapcast "snapclient"} -h ${cfg.client.host} -p ${cfg.client.port} --player alsa -s 3";
      };
    };
  };
}
