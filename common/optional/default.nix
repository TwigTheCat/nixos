{...}: {
  imports = [
    ./users
    ./pipewire.nix
    ./pam.nix
    ./wireless.nix
    ./minecraft.nix
    ./mpd.nix
    ./snapcast.nix
  ];
}
