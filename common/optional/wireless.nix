{
  config,
  lib,
  ...
}: let
  inherit (lib) mkEnableOption mkIf;
  cfg = config.twig.wireless;
in {
  options.twig.wireless = {
    enable = mkEnableOption "wireless";
  };

  config = mkIf cfg.enable {
    networking.wireless.iwd = {
      enable = true;
      settings = {
        General = {
          EnableNetworkConfiguration = true;
          AddressRandomization = "once";
        };
        IPv6.Enabled = true;
        Settings = {
          AutoConnect = true;
        };
      };
    };
  };
}
