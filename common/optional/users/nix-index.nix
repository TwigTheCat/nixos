{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.twig.programs.nix-index;
in {
  options.twig.programs.nix-index = {
    enable = lib.mkEnableOption "nix-index";
    fishIntegration = lib.mkEnableOption "fish integration with nix-index command-not-found replacement";
  };
  config = lib.mkIf cfg.enable {
    home.file.".local/bin/nix-command-not-found" = {
      text = ''
        #!/usr/bin/env bash
        source ${pkgs.nix-index}/etc/profile.d/command-not-found.sh
        command_not_found_handle "$@"
      '';

      executable = true;
    };

    programs.fish.functions = lib.mkIf cfg.fishIntegration {
      "__fish_command_not_found_handler" = {
        body = "$HOME/.local/bin/nix-command-not-found $argv";
        onEvent = "fish_command_not_found";
      };
    };
  };
}
