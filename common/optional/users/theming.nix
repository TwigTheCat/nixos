{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.twig.theming;
  fairyForestCss = ''
    @define-color accent_color #F8C8DC;
    @define-color accent_bg_color #F8C8DC;
    @define-color accent_fg_color #0F1010;
    @define-color destructive_color #FCA5A5;
    @define-color destructive_bg_color #F49AC2;
    @define-color destructive_fg_color #0F1010;
    @define-color success_color #BFD7B5;
    @define-color success_bg_color #C1E7E3;
    @define-color success_fg_color #0F1010;
    @define-color warning_color #F5BBA1;
    @define-color warning_bg_color #F5BBB1;
    @define-color warning_fg_color #0F1010;
    @define-color error_color #FCA5A5;
    @define-color error_bg_color #F49AC2;
    @define-color error_fg_color #0F1010;
    @define-color window_bg_color #0F1010;
    @define-color window_fg_color #C9C7CD;
    @define-color view_bg_color #0F1010;
    @define-color view_fg_color #C9C7CD;
    @define-color headerbar_bg_color #0F1010;
    @define-color headerbar_fg_color #C9C7CD;
    @define-color headerbar_border_color #0F1010;
    @define-color headerbar_backdrop_color #0F1010;
    @define-color headerbar_shade_color rgba(0, 0, 0, 0.36);
    @define-color card_bg_color #0F1010;
    @define-color card_fg_color #C9C7CD;
    @define-color card_shade_color rgba(0,0,0,0.36);
    @define-color dialog_bg_color #0F1010;
    @define-color dialog_fg_color #C9C7CD;
    @define-color popover_bg_color #0F1010;
    @define-color popover_fg_color #C9C7CD;
    @define-color shade_color rgba(0,0,0,0.36);
    @define-color scrollbar_outline_color rgba(0,0,0,0.5);
  '';
in {
  options.twig.theming = {
    enable = lib.mkEnableOption "theming";

    fonts = let
      fontType = lib.types.submodule {
        options = {
          name = lib.mkOption {
            type = lib.types.nullOr lib.types.str;
            default = null;
          };
          package = lib.mkOption {
            type = lib.types.nullOr lib.types.package;
            default = null;
          };
        };
      };
    in {
      enable = lib.mkEnableOption "fonts";

      monospaceFont = lib.mkOption {
        default = {
          name = "CaskaydiaCove Nerd Font";
          package = pkgs.nerd-fonts.caskaydia-cove;
        };
        type = lib.types.nullOr fontType;
      };

      serifFont = lib.mkOption {
        default = {
          name = "Alegreya";
          package = pkgs.alegreya;
        };
        type = lib.types.nullOr fontType;
      };

      sansSerifFont = lib.mkOption {
        default = {
          name = "Inter";
          package = pkgs.inter;
        };
        type = lib.types.nullOr fontType;
      };
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = lib.mkMerge [
      (lib.mkIf cfg.fonts.enable [
        cfg.fonts.monospaceFont.package
        cfg.fonts.serifFont.package
        cfg.fonts.sansSerifFont.package
      ])
      [
        pkgs.nerd-fonts.symbols-only
        pkgs.noto-fonts
        pkgs.noto-fonts-cjk-sans
        pkgs.noto-fonts-color-emoji
      ]
    ];
    fonts.fontconfig = lib.mkIf cfg.fonts.enable {
      enable = true;
      defaultFonts = {
        monospace = [cfg.fonts.monospaceFont.name];
        sansSerif = [cfg.fonts.sansSerifFont.name];
        serif = [cfg.fonts.serifFont.name];
      };
    };
    home.pointerCursor = {
      gtk.enable = true;
      x11.enable = true;
      package = pkgs.phinger-cursors;
      name = "phinger-cursors-dark";
      size = 24;
    };
    gtk = {
      enable = true;
      font = lib.mkIf cfg.fonts.enable {
        inherit (cfg.fonts.sansSerifFont) name package;
        size = 12;
      };
      theme = {
        name = "adw-gtk3-dark";
        package = pkgs.adw-gtk3;
      };
      iconTheme = {
        name = "Papirus-Dark";
        package = pkgs.papirus-icon-theme;
      };
      gtk4.extraCss = fairyForestCss;
      gtk3.extraCss = fairyForestCss;
    };
  };
}
