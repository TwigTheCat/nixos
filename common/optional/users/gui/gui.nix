{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.gui;
  mkAppOption = name:
    lib.mkOption {
      type = lib.types.bool;
      default = cfg.enable;
      description = "install ${name} (enabled by default)";
    };
in {
  options.twig.programs.gui = {
    enable = lib.mkEnableOption "gui applications";

    clapper.enable = mkAppOption "clapper";
    kodi.enable = mkAppOption "kodi";

    art.enable = mkAppOption "art applications";
    downloading.enable = mkAppOption "downloading applications";
  };

  config = lib.mkIf cfg.enable {
    twig.programs = {
      art.enable = lib.mkDefault cfg.art.enable;
      downloading.enable = lib.mkDefault cfg.downloading.enable;
      kodi.enable = lib.mkDefault cfg.kodi.enable;
    };

    home.packages = [
      (lib.mkIf cfg.clapper.enable pkgs.clapper)
    ];
  };
}
