{
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.art;
in {
  options.twig.programs.art = {
    enable = lib.mkEnableOption "art applications";

    krita.enable = lib.mkOption {
      type = lib.types.bool;
      default = cfg.enable;
      description = "install krita (enabled by default)";
    };
    gimp.enable = lib.mkOption {
      type = lib.types.bool;
      default = cfg.enable;
      description = "install gimp (enabled by default)";
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      (lib.mkIf cfg.krita.enable pkgs.krita)
      (lib.mkIf cfg.gimp.enable pkgs.gimp)
    ];
  };
}
