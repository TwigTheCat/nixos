{...}: {
  imports = [
    ./art.nix
    ./downloading.nix
    ./gui.nix
    ./kodi.nix
    ./terminals
  ];
}
