{
  lib,
  config,
  ...
}: let
  cfg = config.twig.programs.kodi;
in {
  options.twig.programs.kodi = {
    enable = lib.mkEnableOption "kodi";
  };

  config = lib.mkIf cfg.enable {
    programs.kodi = {
      enable = true;
    };
  };
}
