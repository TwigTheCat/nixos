{
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.downloading;
in {
  options.twig.programs.downloading = {
    enable = lib.mkEnableOption "applications that download things";

    nicotine.enable = lib.mkOption {
      type = lib.types.bool;
      default = cfg.enable;
      description = "install nicotine+ (enabled by default)";
    };
    transmission.enable = lib.mkOption {
      type = lib.types.bool;
      default = cfg.enable;
      description = "install transmission (enabled by default)";
    };
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      (lib.mkIf cfg.transmission.enable pkgs.transmission_4-gtk)
      (lib.mkIf cfg.nicotine.enable pkgs.nicotine-plus)
    ];
  };
}
