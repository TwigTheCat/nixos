{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.ghostty;
in {
  options.twig.programs.ghostty = {
    enable = lib.mkEnableOption "ghostty terminal";
  };
  config = lib.mkIf cfg.enable {
    programs.ghostty = {
      enable = true;
      settings = {
        theme = "fairy-forest";
        cursor-invert-fg-bg = true;
        font-family = [
          config.twig.theming.fonts.monospaceFont.name
          "Symbols Nerd Font Mono"
        ];
        font-size = 14;
        window-padding-x = 10;
        window-padding-y = 10;
        window-decoration = false;
        gtk-single-instance = false;
        quit-after-last-window-closed = false;
        clipboard-paste-protection = false;
        confirm-close-surface = false;
        resize-overlay = "never";
      };
      themes = {
        fairy-forest = {
          background = "#0F1010";
          cursor-color = "#E1DBEB";
          foreground = "#C9C7CD";
          palette = [
            "0=#0F1010"
            "1=#F8C8DC"
            "2=#BFD7B5"
            "3=#F5BBA1"
            "4=#8EB6F5"
            "5=#F49AC2"
            "6=#C1E7E3"
            "7=#E1DBEB"
            "8=#2A2A2A"
            "9=#F8C8DC"
            "10=#BFD7B5"
            "11=#F5BBA1"
            "12=#8EB6F5"
            "13=#F49AC2"
            "14=#C1E7E3"
            "15=#E1DBEB"
          ];
        };
      };
    };
  };
}
