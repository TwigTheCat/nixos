{...}: {
  imports = [
    ./schizofox.nix
    ./librewolf.nix
  ];
}
