{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.librewolf;
in {
  options.twig.programs.librewolf = {
    enable =
      lib.mkEnableOption "librewolf";
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      pkgs.librewolf
    ];
  };
}
