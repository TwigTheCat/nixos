{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.schizofox;
in {
  options.twig.programs.schizofox = {
    enable = lib.mkEnableOption "schizofox";
  };

  imports = [
    inputs.schizofox.homeManagerModule
  ];

  config = lib.mkIf cfg.enable {
    programs.schizofox = {
      enable = true;

      extensions = {
        enableDefaultExtensions = true;
        enableExtraExtensions = true;
        darkreader.enable = false;
        extraExtensions = {
          "sponsorBlocker@ajay.app".install_url = "https://addons.mozilla.org/firefox/downloads/latest/sponsorblock/latest.xpi";
          "{76ef94a4-e3d0-4c6f-961a-d38a429a332b}".install_url = "https://addons.mozilla.org/firefox/downloads/latest/ttv-lol-pro/latest.xpi";
          "addon@darkreader.org".install_url = "https://addons.mozilla.org/firefox/downloads/latest/darkreader/latest.xpi";
          "7esoorv3@alefvanoon.anonaddy.me".install_url = "https://addons.mozilla.org/firefox/downloads/latest/libredirect/latext.xpi";
        };
      };

      theme = {
        colors = {
          background-darker = "0F1010";
          background = "2A2A2D";
          foreground = "C9C7CD";
          primary = "F8C8DC";
          # border = "F8C8DC";
        };

        font = config.twig.theming.fonts.sansSerifFont.name;

        extraUserChrome = builtins.readFile ./userChrome.css;
        extraUserContent = builtins.readFile ./userContent.css;
      };

      misc = {
        contextMenu.enable = true;
        drm.enable = true;
        disableWebgl = false;
      };

      security = {
        sandbox.enable = false;
      };

      settings = {
        "browser.startup.homepage" = "about:newtab";
        "browser.display.background_color " = "#0F1010";
        "browser.tabs.tabmanager.enabled" = false;
        "security.fileuri.strict_origin_policy" = false;
      };

      search = {
        defaultSearchEngine = "Searx";
        removeEngines = ["Google" "Bing" "Amazon.com" "eBay" "Twitter" "Wikipedia"];
        searxUrl = "https://search.inetol.net/";
        searxQuery = "https://search.inetol.net/search?q={searchTerms}";

        addEngines = [
          {
            Name = "Brave";
            Alias = "!B";
            Method = "GET";
            URLTemplate = "https://search.brave.com/search?q={searchTerms}&source=web";
          }
        ];
      };
    };
  };
}
