{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.ungoogled-chromium;
in {
  options.twig.programs.ungoogled-chromium = {
    enable =
      lib.mkEnableOption "ungoogled-chromium";
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      pkgs.ungoogled-chromium
    ];
  };
}
