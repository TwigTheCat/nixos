{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.helix;
in {
  options.twig.programs.helix = {
    enable = lib.mkEnableOption "helix";
  };
  config = lib.mkIf cfg.enable {
    programs.helix = {
      enable = true;
      settings = {
        theme = "fairy_forest";
        editor = {
          soft-wrap.enable = true;
          line-number = "relative";
          auto-completion = true;
          auto-format = true;
          color-modes = true;
          idle-timeout = 50;
          lsp.display-inlay-hints = false;
          lsp.display-messages = true;
          cursor-shape = {
            select = "underline";
            insert = "bar";
            normal = "block";
          };
          file-picker.hidden = false;
        };
        keys.normal = {
          y = [":clipboard-yank" "yank"];
          C-right = "move_next_long_word_start";
          C-left = "move_prev_long_word_start";
          S-right = "move_next_word_start";
          S-left = "move_prev_word_start";
          C-up = "goto_prev_paragraph";
          C-down = "goto_next_paragraph";
          S-up = "move_line_up";
          S-down = "move_line_down";
        };
        keys.select = {
          y = [":clipboard-yank" "yank"];
          C-right = "move_next_long_word_start";
          C-left = "move_prev_long_word_start";
          S-right = "move_next_word_start";
          S-left = "move_prev_word_start";
          C-up = "goto_prev_paragraph";
          C-down = "goto_next_paragraph";
        };
        keys.insert = {
          C-right = "move_next_long_word_start";
          C-left = "move_prev_long_word_start";
          S-right = "move_next_word_start";
          S-left = "move_prev_word_start";
          C-up = "goto_prev_paragraph";
          C-down = "goto_next_paragraph";
          C-backspace = "delete_word_backward";
        };
      };
      languages = {};
    };
  };
}
