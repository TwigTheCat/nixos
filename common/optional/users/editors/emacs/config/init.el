;;; -*- lexical-binding: t; -*-

;; STRAIGHT USE-PACKAGE

(require 'package)
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
        "straight/repos/straight.el/bootstrap.el"
        (or (bound-and-true-p straight-base-dir)
            user-emacs-directory)))
      (bootstrap-version 7))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq use-package-always-ensure nil)
(straight-use-package 'use-package)

;; KEYBINDS

(load (expand-file-name "keybind.el" user-emacs-directory))

;; LSP/TREESITTER, ETC

(load (expand-file-name "language.el" user-emacs-directory))

;; NOTES/ORG-MODE

(load (expand-file-name "./notes.el" user-emacs-directory))

;; TEXT STYLING

(load (expand-file-name "./text-style.el" user-emacs-directory))

;; PROGRAMS

(load (expand-file-name "./programs.el" user-emacs-directory))

;; PERIPHERALS

(load (expand-file-name "./peripherals.el" user-emacs-directory))

;; MISC

(load (expand-file-name "./misc.el" user-emacs-directory))

;; THEMING

(load (expand-file-name "./theming.el" user-emacs-directory))


;; NIX MANAGED CONFIG
(load (expand-file-name "./nix-managed.el" user-emacs-directory))

