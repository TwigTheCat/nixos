;;; -*- lexical-binding: t; -*-

;; DOOM THEME EXTRAS

(doom-themes-visual-bell-config)

;; CUSTOM

(custom-set-faces
 '(org-level-1 ((t (:inherit outline-1 :height 1.1))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.1))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.1))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.1))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.1))))
 '(org-level-6 ((t (:inherit outline-5 :height 1.1))))
 '(org-level-7 ((t (:inherit outline-5 :height 1.1)))))

(custom-set-variables
 '(custom-safe-themes
   '("4dd267a7349de7b377c73a8fc7ea99a8e90ddecbc88bd8a731331f5dd44ae596"
     default))
 '(package-selected-packages nil))
