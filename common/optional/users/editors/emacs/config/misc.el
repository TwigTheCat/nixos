;;; -*- lexical-binding: t; -*-

;; GARBAGE COLLECTION

(use-package gcmh
  :config
  (gcmh-mode 1)
  (setq gcmh-cons-threshold 100000000))

;; NO-LITTERING

(use-package no-littering
  :straight t
  :config
  (require 'no-littering))

(defvar autosave-dir (concat "~/.config/emacs/auto-save" "/"))
(make-directory autosave-dir t)
(setq auto-save-file-name-transforms
      `(("\\(?:[^/]*/\\)*\\(.*\\)" ,(concat autosave-dir "\\1") t)))

;; BACKUPS

(defun twig/backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* ((backupRootDir "~/.config/emacs/emacs-backup/")
         (backupFilePath (replace-regexp-in-string "//" "/" (concat backupRootDir filePath "~") )))
    (make-directory (file-name-directory backupFilePath) (file-name-directory backupFilePath))
    backupFilePath))
(setq make-backup-file-name-function 'twig/backup-file-name)

;; DRAG-STUFF

(use-package drag-stuff
  :defer t
  :config
  (drag-stuff-global-mode 1))

;; SCROLLING

(setq scroll-conservatively 101)
(setq pixel-scroll-precision-large-scroll-height 40.0)
(pixel-scroll-precision-mode 1)

;; (setopt pixel-scroll-precision-interpolate-mice t)
;; (setopt pixel-scroll-precision-use-momentum t)
;; (setopt pixel-scroll-precision-interpolate-page t)

;; GTK

(menu-bar-mode -1)
(tool-bar-mode -1)
(blink-cursor-mode -1)

;; LINE NUMBERS

(setq display-line-numbers-type 'relative)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'text-mode-hook 'visual-line-mode)

;; EXTERNAL CHANGES

(global-auto-revert-mode t)

;; SELECTION DELETE

(delete-selection-mode 1)


;; EMACS PACKAGE SETTINGS

(use-package emacs
  :custom
  (text-mode-ispell-word-completion nil)
  (read-extended-command-predicate #'command-completion-default-include-p))
