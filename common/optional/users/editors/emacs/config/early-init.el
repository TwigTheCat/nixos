;; THEME

(add-to-list 'custom-theme-load-path "~/.config/emacs/themes/")
(use-package doom-themes
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'fairy-forest t))

;; FONT

(setq-default line-spacing nil)

(set-face-attribute
 'default nil
 :family "Monaspace Neon Frozen"
 :height 120
 :weight 'semi-bold)

(set-fontset-font t nil (font-spec :size 12 :name "Symbols Nerd Font Mono"))
(add-hook 'server-after-make-frame-hook
            `(lambda ()
               (set-fontset-font t nil (font-spec :size 12 :name "Symbols Nerd Font Mono"))))
