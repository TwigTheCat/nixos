;;; -*- lexical-binding: t; -*-

;; YES/Y NO/N

(fset 'yes-or-no-p 'y-or-n-p)

;; WINDMOVE

(windmove-default-keybindings)

;; OTHER KEYBINDS

;; search
(defvar-keymap search-keymap
  "g" #'consult-ripgrep
  "f" #'consult-fd)
(defalias 'search-keymap search-keymap)
(define-key global-map (kbd "C-c f") #'search-keymap)

;; buffers
(defvar-keymap buffer-keymap
  "k" #'kill-current-buffer
  "b" #'switch-to-buffer
  "n" #'next-buffer
  "p" #'previous-buffer
  "r" #'revert-buffer)
(defalias 'buffer-keymap buffer-keymap)
(define-key global-map (kbd "C-c b") #'buffer-keymap)

;; spellcheck
(define-key global-map (kbd "C-c s c") #'ispell-word)

;; goto
;; NOTE: when doing leader-g with meow, itll interpret the keybind as C-M- and we can't really make a map for C-M, so we just bind it like this
(global-set-key (kbd "C-M-d") #'xref-find-definitions)

;; zoom in and out
(global-set-key (kbd "C-=") #'text-scale-increase)
(global-set-key (kbd "C--") #'text-scale-decrease)
(global-set-key (kbd "<C-wheel-up>") #'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") #'text-scale-decrease)

;; (global-set-key [wheel-down] #'scroll-up-line)
;; (global-set-key [wheel-up] #'scroll-down-line)

;; drag stuff
(global-set-key (kbd "M-<up>") #'drag-stuff-up)
(global-set-key (kbd "M-<down>") #'drag-stuff-down)

;; man fuck minibuffers and their 3 esc quits
(global-set-key [escape] #'keyboard-escape-quit)
(global-set-key (kbd "C-c c") #'comment-line)

;; MEOW

(use-package meow
  :config
  (require 'meow)
  (defun meow-setup ()
    (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
    (setq meow-use-clipboard t)
    (meow-motion-overwrite-define-key
     '("j" . meow-next)
     '("k" . meow-prev)
     '("<escape>" . ignore))
    (meow-leader-define-key
     ;; SPC j/k will run the original command in MOTION state.
     '("j" . "H-j")
     '("k" . "H-")
     ;; Use SPC (0-9) for digit arguments.
     '("1" . meow-digit-argument)
     '("2" . meow-digit-argument)
     '("3" . meow-digit-argument)
     '("4" . meow-digit-argument)
     '("5" . meow-digit-argument)
     '("6" . meow-digit-argument)
     '("7" . meow-digit-argument)
     '("8" . meow-digit-argument)
     '("9" . meow-digit-argument)
     '("0" . meow-digit-argument)
     '("/" . meow-keypad-describe-key)
     '("?" . meow-cheatsheet))
    (meow-normal-define-key
     '("C-r" . undo-redo)
     '("0" . meow-expand-0)
     '("9" . meow-expand-9)
     '("8" . meow-expand-8)
     '("7" . meow-expand-7)
     '("6" . meow-expand-6)
     '("5" . meow-expand-5)
     '("4" . meow-expand-4)
     '("3" . meow-expand-3)
     '("2" . meow-expand-2)
     '("1" . meow-expand-1)
     '("-" . negative-argument)
     '(";" . meow-reverse)
     '("," . meow-inner-of-thing)
     '("." . meow-bounds-of-thing)
     '("[" . meow-beginning-of-thing)
     '("]" . meow-end-of-thing)
     '("a" . meow-append)
     '("A" . meow-open-below)
     '("b" . meow-back-word)
     '("B" . meow-back-symbol)
     '("c" . meow-change)
     '("d" . meow-delete)
     '("D" . meow-backward-delete)
     '("e" . meow-next-word)
     '("E" . meow-next-symbol)
     '("f" . meow-find)
     '("g" . meow-cancel-selection)
     '("G" . meow-grab)
     '("h" . meow-left)
     '("H" . meow-left-expand)
     '("i" . meow-insert)
     '("I" . meow-open-above)
     '("j" . meow-next)
     '("J" . meow-next-expand)
     '("k" . meow-prev)
     '("K" . meow-prev-expand)
     '("l" . meow-right)
     '("L" . meow-right-expand)
     '("m" . meow-join)
     '("n" . meow-search)
     '("o" . meow-block)
     '("O" . meow-to-block)
     '("p" . meow-yank)
     '("q" . meow-quit)
     '("Q" . meow-goto-line)
     '("r" . meow-replace)
     '("R" . meow-swap-grab)
     '("s" . meow-kill)
     '("t" . meow-till)
     '("u" . meow-undo)
     '("U" . meow-undo-in-selection)
     '("v" . meow-visit)
     '("w" . meow-mark-word)
     '("W" . meow-mark-symbol)
     '("x" . meow-line)
     '("X" . meow-goto-line)
     '("y" . meow-save)
     '("Y" . meow-sync-grab)
     '("z" . meow-pop-selection)
     '("'" . repeat)
     '("<escape>" . ignore)))
  (meow-setup)
  (meow-global-mode 1))

(use-package meow-tree-sitter
  :config
  (require 'meow-tree-sitter)
  (meow-tree-sitter-register-defaults))
