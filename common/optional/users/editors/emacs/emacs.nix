{
  inputs,
  lib,
  config,
  pkgs,
  ...
}: let
  eglot-booster = epkgs:
    epkgs.trivialBuild rec {
      pname = "eglot-booster";
      version = "e6daa6bcaf4aceee29c8a5a949b43eb1b89900ed";
      src = pkgs.fetchFromGitHub {
        owner = "jdtsmith";
        repo = pname;
        rev = version;
        hash = "sha256-PLfaXELkdX5NZcSmR1s/kgmU16ODF8bn56nfTh9g6bs=";
      };
    };

  tip = epkgs:
    epkgs.melpaBuild rec {
      pname = "tip";
      version = "0.0.0";
      src = pkgs.fetchgit {
        url = "https://git.sr.ht/~mafty/${pname}";
        rev = "2ccfe1e7f9675ed612275d88f046fead0bb9dc6d";
        hash = "sha256-G2GxnvSFz35Ay8FNdAfUF+Nvhf25xlEm6vKJQtN/350=";
      };
    };

  typst-ts-mode = epkgs:
    epkgs.melpaBuild rec {
      pname = "typst-ts-mode";
      version = "0.0.0";
      src = pkgs.fetchgit {
        url = "https://codeberg.org/meow_king/${pname}";
        rev = "aee8ff090cff6f39a70ac2c9035b1eb2b69924d7";
        hash = "sha256-oZ4uq87PeqXGIy8Dmx7ppGzrxDhiJEGOH8nXerZ03ik=";
      };
    };

  zig-ts-mode = epkgs:
    epkgs.melpaBuild rec {
      pname = "zig-ts-mode";
      version = "0.0.0";
      src = pkgs.fetchgit {
        url = "https://codeberg.org/meow_king/${pname}";
        rev = "020500ac3c9ac2cadedccb5cd6c506eb38327443";
        hash = "sha256-vstl13IWwAxaQTsy/bn/uCet4Oxm2edKjmwREfhNAk8=";
      };
    };

  # https://git.eriedaberrie.me/eriedaberrie/dotfiles/src/commit/8539e70ee8f063e2673b26b5654edc8be4e86810/modules/home/graphical/emacs.nix#L12
  extraEpkgsMap = epkgs: extraEpkgList: map (extraEpkg: extraEpkg epkgs) extraEpkgList;

  myEmacs =
    (pkgs.emacsPackagesFor
      pkgs.emacs-pgtk)
    .emacsWithPackages
    (epkgs:
      (builtins.attrValues {
        inherit
          (epkgs.melpaPackages)
          vterm
          no-littering
          gcmh
          # evil
          # evil-collection
          meow
          meow-tree-sitter
          direnv
          general
          org-superstar
          toc-org
          org-auto-tangle
          cdlatex
          org-fragtog
          org-appear
          dashboard
          consult
          embark
          embark-consult
          corfu
          cape
          yasnippet
          vertico
          marginalia
          orderless
          projectile
          rainbow-delimiters
          neotree
          nerd-icons
          nerd-icons-dired
          vterm-toggle
          hl-todo
          drag-stuff
          ligature
          transient
          magit
          hindent
          rust-mode
          yaml-mode
          json-mode
          haskell-mode
          v-mode
          nix-mode
          nix-ts-mode
          tuareg
          dune
          merlin
          utop
          ocp-indent
          markdown-mode
          reformatter
          eldoc-box
          treesit-auto
          doom-themes
          ;
        inherit
          (epkgs)
          auctex
          denote
          kind-icon
          nano-modeline
          rainbow-mode
          ;
        inherit (epkgs.treesit-grammars) with-all-grammars;
        inherit (inputs.nix-qml.packages.${pkgs.system}) qml-ts-mode tree-sitter-qmljs;
      })
      ++ extraEpkgsMap epkgs [
        eglot-booster
        tip
        typst-ts-mode
        zig-ts-mode
      ]);

  cfg = config.twig.programs.emacs;
in {
  options.twig.programs.emacs = {
    enable = lib.mkEnableOption "emacs service";
    client.enable = lib.mkEnableOption "emacs client";
    standalone.enable = lib.mkEnableOption "emacs standalone";
  };

  config = lib.mkIf cfg.enable {
    xdg.configFile = {
      "emacs/themes".source = ./config/themes;
      "emacs/dashboard.txt".source = ./config/dashboard.txt;
      "emacs/init.el".source = ./config/init.el;
      "emacs/early-init.el".source = ./config/early-init.el;
      "emacs/keybind.el".source = ./config/keybind.el;
      "emacs/language.el".source = ./config/language.el;
      "emacs/misc.el".source = ./config/misc.el;
      "emacs/notes.el".source = ./config/notes.el;
      "emacs/peripherals.el".source = ./config/peripherals.el;
      "emacs/programs.el".source = ./config/programs.el;
      "emacs/text-style.el".source = ./config/text-style.el;
      "emacs/theming.el".source = ./config/theming.el;
      "emacs/nix-managed.el".text = let
        qml-treesitter = inputs.nix-qml.packages.${pkgs.system}.tree-sitter-qmljs.overrideAttrs (final: prev: {
          fixupPhase = ''
            ln -s $out/parser $out/libtree-sitter-qmljs.so
          '';
        });
      in ''
        (add-to-list 'treesit-extra-load-path "${qml-treesitter}")
      '';
    };

    services.emacs = {
      enable = true;
      client.enable = cfg.client.enable;
      package = myEmacs;
    };
    programs.emacs = lib.mkIf cfg.standalone.enable {
      enable = true;
      package = myEmacs;
    };
    home.packages = [
      pkgs.typst
      (pkgs.aspellWithDicts (ds: with ds; [en en-science en-computers]))
      pkgs.emacs-lsp-booster
      # inputs.nix-qml.packages.${pkgs.system}.tree-sitter-qmljs
    ];
  };
}
