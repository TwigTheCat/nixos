{
  pkgs,
  config,
  lib,
  inputs,
  ...
}: let
  cfg = config.twig.programs.nvf;
in {
  options.twig.programs.nvf = {
    enable = lib.mkEnableOption "nvf";
  };

  imports = [
    inputs.nvf.homeManagerModules.default
  ];

  config = lib.mkIf cfg.enable {
    programs.nvf = {
      enable = true;
      enableManpages = true;
      settings = {
        vim = {
          package = inputs.nvim-nightly.packages.${pkgs.system}.default;

          enableLuaLoader = true;
          viAlias = true;
          vimAlias = true;

          undoFile.enable = true;

          extraPlugins = {
            friendly-snippets = {
              package = pkgs.vimPlugins.friendly-snippets;
            };
            rainbowDelims = {
              package = pkgs.vimPlugins.rainbow-delimiters-nvim;
              setup =
                /*
                lua
                */
                ''
                  vim.api.nvim_set_hl(0, "RainbowDelimiterRed", {fg = "#F49AC2"})
                  vim.api.nvim_set_hl(0, "RainbowDelimiterYellow", {fg = "#F5BBA1"})
                  vim.api.nvim_set_hl(0, "RainbowDelimiterBlue", {fg = "#8EB6F5"})
                  vim.api.nvim_set_hl(0, "RainbowDelimiterGreen", {fg = "#BFD7B5"})
                  vim.api.nvim_set_hl(0, "RainbowDelimiterViolet", {fg = "#C3B1E1"})
                  vim.api.nvim_set_hl(0, "RainbowDelimiterCyan", {fg = "#C1E7E3"})
                  require('rainbow-delimiters.setup').setup {
                    strategy = {

                    },
                    query = {

                    },
                    highlight = {
                      'RainbowDelimiterRed',
                      'RainbowDelimiterYellow',
                      'RainbowDelimiterBlue',
                      'RainbowDelimiterGreen',
                      'RainbowDelimiterViolet',
                      'RainbowDelimiterCyan',
                    },
                  }
                '';
            };
          };

          # so like, whenever you have the zig-nvim autosave enabled,
          # it kinda inserts random junk date into the beginning of the file,
          # however, the lsp also auto formats on save so disabling it is fine.
          luaConfigRC.basic = ''
            vim.g.zig_fmt_autosave = 0;
          '';

          useSystemClipboard = true;

          options = {
            cursorline = true;
            shiftwidth = 2;
            tabstop = 2;
            softtabstop = 2;
          };
          globals = {
            mapleader = " ";
            maplocalleader = " ";
          };

          lineNumberMode = "relative";

          autopairs.nvim-autopairs.enable = true;

          binds = {
            whichKey.enable = true;
          };

          notes.todo-comments.enable = true;

          ui = {
            borders.plugins = {
              nvim-cmp = {
                enable = true;
                style = "single";
              };
            };
            colorizer = {
              enable = true;
              setupOpts = {
                filetypes."*" = {};
                user_default_options = {
                  AARRGGBB = false;
                  RGB = true;
                  RRGGBB = true;
                  RRGGBBAA = true;
                  mode = "background";
                  css = true;
                  names = false;
                  rgb_fn = true;
                  css_fn = true;
                  hsl_fn = true;
                };
              };
            };
          };

          telescope = {
            enable = true;
          };

          comments.comment-nvim = {
            enable = true;
            mappings = {
              toggleCurrentLine = "<C-c>";
              toggleSelectedLine = "<C-c>";
            };
          };

          theme = {
            enable = true;
            name = "base16";
            base16-colors = {
              base00 = "#0F1010";
              base01 = "#1B1B1B";
              base02 = "#2A2A2A";
              base03 = "#3E3E43";
              base04 = "#57575F";
              base05 = "#C9C7CD";
              base06 = "#F8C8DC";
              base07 = "#C3B1E1";
              base08 = "#F39AC2";
              base09 = "#F5BBA1";
              base0A = "#F5BBA1";
              base0B = "#BFD7B5";
              base0C = "#C1E7E3";
              base0D = "#8EB6F5";
              base0E = "#C3B1E1";
              base0F = "#E1DBEB";
            };
          };

          lsp = {
            enable = true;
            formatOnSave = true;

            lspkind.enable = true;
            lsplines.enable = true;

            mappings = {
              goToDeclaration = "gd";
              goToDefinition = "gd";
            };
          };
          autocomplete.blink-cmp = {
            enable = true;
          };

          snippets = {
            luasnip.enable = true;
          };

          languages = {
            enableTreesitter = true;
            enableExtraDiagnostics = true;
            enableFormat = true;
            enableLSP = true;

            zig.enable = true;
            nix.enable = true;
            rust.enable = true;
            go.enable = true;
            lua.enable = true;
            java.enable = true;
          };
        };
      };
    };
  };
}
