{...}: {
  imports = [
    ./emacs
    ./nvf.nix
    ./helix.nix
  ];
}
