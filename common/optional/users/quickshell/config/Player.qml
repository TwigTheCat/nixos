import QtQuick
import QtQuick.Shapes
import Quickshell.Services.Mpris

Rectangle {
  required property string song
  required property int playerIdx 
  
  width:  playerText.width + 20
  height: 35
  color: "#FF2A2A2A"
  border.color: "#FF8EB7F5"
  border.width: 3
  
  Text {
    id: playerText
    font.family: "Monaspace Radon"
    font.pointSize: 13
    font.weight: 600

    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter

    color: "#FFE1DBEB"
    text: song
  }
}
