import QtQuick
import Quickshell.Hyprland

ListView {
  required property var workspaceArray
  
  model: workspaceArray.filter((i) => i.visible)
  spacing: 4
  height: width * workspaceArray.length
  delegate: Rectangle {
    id: workspace
    visible: modelData.visible
    border.color: (modelData.active) ? "#FFF8C8DC" : "#FFE1DBEB"
    border.width: 3
    anchors.horizontalCenter: parent.horizontalCenter
    width: 35
    height: 35
    color: "#FF2A2A2A"
    Text {
      id: workspaceText
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.verticalCenter: parent.verticalCenter
      font.family: "Symbols Nerd Font Mono"
      font.pointSize: 13
      font.weight: 600
      color: (modelData.active) ? "#FFF8C8DC" : "#FFE1DBEB"
      text: modelData.text
    }
    MouseArea {
      id: mouseArea
      anchors.fill: parent
      hoverEnabled: true
      onClicked: {
        Hyprland.dispatch("workspace " + modelData.id)
      }
    }
    states: State {
      name: "hovered"; when: (mouseArea.containsMouse && !modelData.active)
      PropertyChanges {
        target: workspace
        border.color: "#FF0F1010"
      }
      PropertyChanges {
        target: workspaceText
        color: "#FF9998A8"
      }
    }
    transitions: Transition {
      ColorAnimation {
        duration: 200
      }
    }
  }
}
