import Quickshell
import Quickshell.Services.Mpris
import Quickshell.Services.Pipewire
import Quickshell.Services.UPower
import Quickshell.Hyprland
import Quickshell.Io

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick.Shapes

ShellRoot {
  id: root

  Variants {
    model: Quickshell.screens

    Item {
      id: windowContainer
      property var modelData

      PanelWindow {
        id: sideBar

        screen: windowContainer.modelData
        anchors {
          top: true
          left: true
          bottom: true
        }
        width: 35
        color: "#FF0F1010"
        
        HyprlandWorkspaces {
          width: parent.width
          anchors.top: parent.top
          anchors.horizontalCenter: parent.horizontalCenter
          anchors.topMargin: 4

          //TODO: this should be initialized in the HyprlandWorkspaces type
          workspaceArray: Array.from({ length: 7 }, (_, i) => ({
            id: i + 1,
            text: getWorkspaceIcon((i + 1)),
            visible: Hyprland.workspaces.values.some(e => e.id === i + 1),
            active: Hyprland.focusedMonitor.activeWorkspace.id === i + 1
          }))
  
          function getWorkspaceIcon(id: int): string {
            switch (id) {
            case 1: return "󱆃"
            case 2: return ""
            case 3: return "󰈹"
            case 4: return "󰙯"
            case 5: return "󰎈"
            case 6: return "󱎓"
            case 7: return ""
            default: return "sdfkjsdklfjsdkl"
            }
          }
        }

        Column {
          id: progressColumn
          property int childHeight: 35
          
          width: 35
          height: childRect.height
          anchors.bottom: parent.bottom

          SquareProgress {
            id: spkVolume
            anchors.bottom: micVolume.top
            anchors.bottomMargin: 4
            height: progressColumn.childHeight
            width: 35

            required property PwNode spkTracker
            spkTracker: Pipewire.defaultAudioSink

            function getIcon(muted: bool): string {
              switch (muted) {
              case true: return "󰖁"
              case false: return "󰕾"
              default: return "wtf"
               }
            }

            PwObjectTracker {
              objects: [ spkVolume.spkTracker ]
            }

            MouseArea {
              anchors.fill: parent
              onClicked: {
                spkVolume.spkTracker.audio.muted = !spkVolume.spkTracker.audio.muted
              }

              onWheel: {
                if (wheel.angleDelta.y < 0) {
                  if (spkVolume.spkTracker.audio.volume - 0.05 >= 0) {
                    spkVolume.spkTracker.audio.volume -= 0.05
                  } else {
                    spkVolume.spkTracker.audio.volume = 0
                  }
                } else if (wheel.angleDelta.y > 0) {
                  if (spkVolume.spkTracker.audio.volume + 0.05 <= 1) {
                    spkVolume.spkTracker.audio.volume += 0.05
                  } else {
                    spkVolume.spkTracker.audio.volume = 1
                  }
                }
              }
            }

            icon: {
              spkVolume.spkTracker = Pipewire.defaultAudioSink
              getIcon(spkVolume.spkTracker.audio.muted)
            }

            backgroundColor: "#FF2A2A2A"
            borderColor: "#FF8EB6F5"
            borderHoveredColor: "#FF0F1010"
            iconColor:"#FF8EB6F5"
            iconHoveredColor:"#FF9998A8"

            percent: spkVolume.spkTracker.audio.volume
          }

          SquareProgress {
            id: micVolume
            anchors.bottom: battery.top
            anchors.bottomMargin: 4
            height: progressColumn.childHeight
            width: 35

            required property PwNode micTracker
            micTracker: Pipewire.defaultAudioSource

            function getIcon(muted: bool): string {
              switch (muted) {
              case true: return "󰍭"
              case false: return "󰍬"
              default: return "wtf"
              }
            }

            PwObjectTracker {
              objects: [ micVolume.micTracker ]
            }

            MouseArea {
              anchors.fill: parent
              onClicked: {
                micVolume.micTracker.audio.muted = !micVolume.micTracker.audio.muted
              }

              onWheel: {
                if (wheel.angleDelta.y < 0) {
                  if (micVolume.micTracker.audio.volume - 0.05 >= 0) {
                    micVolume.micTracker.audio.volume -= 0.05
                  } else {
                    micVolume.micTracker.audio.volume = 0
                  }
                } else if (wheel.angleDelta.y > 0){
                  if (micVolume.micTracker.audio.volume + 0.05 <= 1) {
                    micVolume.micTracker.audio.volume += 0.05
                  } else {
                    micVolume.micTracker.audio.volume = 1
                  }
                }
              }
            }

            icon: {
              micVolume.micTracker = Pipewire.defaultAudioSource
              getIcon(micVolume.micTracker.audio.muted)
            }

            backgroundColor: "#FF2A2A2A"
            borderColor: "#FF8EB6F5"
            borderHoveredColor: "#FF0F1010"
            iconColor:"#FF8EB6F5"
            iconHoveredColor:"#FF9998A8"

            percent: micVolume.micTracker.audio.volume
          }

          SquareProgress {
            id: battery
            height: progressColumn.childHeight
            width: 35
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 3

            function getIcon(energyPercentage: real, charging: bool): string {
              switch (Math.ceil(energyPercentage / 10) * 10) {
              case 0: return (charging)   ? "󰂄" : "󰂃"
              case 10: return (charging)  ? "󰂄" : "󰁺"
              case 20: return (charging)  ? "󰂄" : "󰁻"
              case 30: return (charging)  ? "󰂄" : "󰁼"
              case 40: return (charging)  ? "󰂄" : "󰁽"
              case 50: return (charging)  ? "󰂄" : "󰁾"
              case 60: return (charging)  ? "󰂄" : "󰁿"
              case 70: return (charging)  ? "󰂄" : "󰂀"
              case 80: return (charging)  ? "󰂄" : "󰂁"
              case 90: return (charging)  ? "󰂄" : "󰂂"
              case 100: return (charging) ? "󰂄" : "󰁹"
              default: return "wtf"
              }
            }
            icon: getIcon(UPower.devices.values[0].energy / UPower.devices.values[0].energyCapacity * 100,
              (UPower.devices.values[0].timeToFull != 0))
            backgroundColor: "#FF2A2A2A"
            borderColor: "#FFF5BBA1"
            borderHoveredColor: "#FF0F1010"
            iconColor: "#FFF5BBA1"
            iconHoveredColor: "#FF9998A8"

            percent: UPower.devices.values[0].energy / UPower.devices.values[0].energyCapacity
          }

        }
      }
      
      PanelWindow {
        id: topBar

        screen: windowContainer.modelData
        anchors {
          top: true
          left: true
          right: true
        }
        height: 40
        color: "#FF0F1010"

        Rectangle {
          height: 35
          anchors.top: parent.top
          anchors.topMargin: 4
          anchors.left: parent.left
          anchors.leftMargin: 3
          Clock {
            id: clock
            height: parent.height
          }
          PlayerControls {
            id: playerControls

            anchors.left: clock.right
            anchors.leftMargin: 3
            height: parent.height

            playerIdx: 0
          }
          Player {
            id: player
            height: parent.height

            anchors.left: playerControls.right
            anchors.leftMargin: 3

            playerIdx: 0
            visible: {
              print (Mpris.players.values[playerIdx])
              return (Mpris.players.values[playerIdx] != undefined && Mpris.players.values[playerIdx].trackArtists != "")
            }

            //TODO: recalculate the idx when mpris emits signals

            song:
              (Mpris.players.values[playerIdx].trackArtists
                + " - "
                + Mpris.players.values[playerIdx].trackTitle
                + " | "
                + Math.floor(Mpris.players.values[playerIdx].position / 60)
                + ":"
                + String(Math.floor(Mpris.players.values[playerIdx].position % 60)).padStart(2, 0)
                + "/"
                + Math.floor(Mpris.players.values[playerIdx].length / 60)
                + ":"
                + String(Math.floor(Mpris.players.values[playerIdx].length % 60)).padStart(2, 0))
            
            Timer {
              // only emit the signal when the position is actually changing.
              running: Mpris.players.values[playerIdx].playbackState == MprisPlaybackState.Playing
              // Make sure the position updates at least once per second.
              interval: 1000
              repeat: true
              // emit the positionChanged signal every second.
              onTriggered: Mpris.players.values[playerIdx].positionChanged()
            }
          }
        }
      }
    }
  }
}
