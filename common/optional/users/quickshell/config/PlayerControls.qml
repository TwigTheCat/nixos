import Quickshell
import Quickshell.Services.Mpris

import QtQuick
import QtQuick.Shapes

Row {
  required property int playerIdx
  property bool playing: Mpris.players.values[playerIdx].playbackState == MprisPlaybackState.Playing
  property var song: Mpris.players.values[playerIdx]

  width: height * 3 + 6
  visible: {
    if (song != undefined) {
      return song.trackArtist != ""
    } else {
      return false
    }
  }
  Rectangle {
    id: prev
    width: parent.height
    height: parent.height

    anchors.left: parent.left

    color: "#FF2A2A2A"
    border.color: "#FF8EB6F5"
    border.width: 3

    Image {
      id: prevSvg
      width: parent.width / 2
      height: width
      anchors.centerIn: parent
      source: "./prev-button.svg"
      mipmap: true
    }

    MouseArea {
      id: prevMouseArea
      anchors.fill: prev
      hoverEnabled: true
      onClicked: {
        Mpris.players.values[playerIdx].previous()
      }
    }
    states: State {
      name: "hovered"
      when: prevMouseArea.containsMouse
      PropertyChanges {
        target: prev
        border.color: "#FF0F1010"
      }
    }
    transitions: Transition {
      ColorAnimation {
        duration: 200
      }
    }
  }
  Rectangle {
    id: play
    width: parent.height
    height: parent.height

    anchors.left: prev.right
    anchors.leftMargin: 3

    color: "#FF2A2A2A"
    border.color: "#FF8EB6F5"
    border.width: 3

    Image {
      id: playSvg
      width: (playing) ? parent.width / 2 : parent.width / 1.5
      height: width
      anchors.centerIn: parent
      source: (playing) ? "./play-button.svg" : "./pause-button.svg"
      mipmap: true
    }
    
    MouseArea {
      id: playMouseArea
      anchors.fill: play
      hoverEnabled: true
      onClicked: {
        Mpris.players.values[playerIdx].togglePlaying()
      }
    }
    states: State {
      name: "hovered"
      when: playMouseArea.containsMouse
      PropertyChanges {
        target: play
        border.color: "#FF0F1010"
      }
    }
    transitions: Transition {
      ColorAnimation {
        duration: 200
      }
    }
  }
  Rectangle {
    id: next
    width: parent.height
    height: parent.height

    anchors.left: play.right
    anchors.leftMargin: 3

    color: "#FF2A2A2A"
    border.color: "#FF8EB6F5"
    border.width: 3

    Image {
      id: forwardSvg
      width: parent.width / 2
      height: width
      anchors.centerIn: parent
      source: "./forward-button.svg"
      mipmap: true
    }

    MouseArea {
      id: nextMouseArea
      anchors.fill: next
      hoverEnabled: true
      onClicked: {
        Mpris.players.values[playerIdx].next()
      }
    }
    states: State {
      name: "hovered"
      when: nextMouseArea.containsMouse
      PropertyChanges {
        target: next
        border.color: "#FF0F1010"
      }
    }
    transitions: Transition {
      ColorAnimation {
        duration: 200
      }
    }
  }
}
