import QtQuick
import QtQuick.Shapes
import QtQuick.Controls
import Quickshell.Io

Rectangle {
  property string time;
  
  Process {
    id: dateProc
    command: ["date", "+%I:%M%p %A, %b %e, %Y"]
    running: true
    stdout: SplitParser {
      onRead: data => time = data
    }
  }
  Timer {
    interval: 500
    running: true
    repeat: true
    onTriggered: dateProc.running = true
  }

  color: "#FF2A2A2A"
  width: childrenRect.width + 20
  border.color: "#FFF8C8DC"
  border.width: 3
  
  Text {
    font.family: "Monaspace Radon"
    font.pointSize: 13
    font.weight: 600
  
    anchors.verticalCenter: parent.verticalCenter
    anchors.horizontalCenter: parent.horizontalCenter
  
    color: "#FFE1DBEB"
    text: time
  }
}
