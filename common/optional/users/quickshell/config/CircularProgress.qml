import QtQuick
import QtQuick.Shapes

Rectangle {
  required property string icon
  
  required property color borderColor
  required property color borderColorHovered
  required property string backgroundColor
  required property string textColorHovered

  required property real percentage 

  color: "transparent"
  Canvas {
    id: canvas
    width: parent.width
    height: width
    anchors.centerIn: parent
    
    property real colorPhase: 1

    antialiasing: true

    onPaint: {
      var ctx = getContext("2d")
      ctx.reset()

      var interpolatedColorR = borderColor.r * (1 - colorPhase) + borderColorHovered.r * colorPhase;
      var interpolatedColorG = borderColor.g * (1 - colorPhase) + borderColorHovered.g * colorPhase;
      var interpolatedColorB = borderColor.b * (1 - colorPhase) + borderColorHovered.b * colorPhase;

      ctx.lineWidth = 3
      ctx.fillStyle = backgroundColor

      var radius = width / 2 - ctx.lineWidth / 2

      ctx.beginPath()
      ctx.strokeStyle = backgroundColor
      ctx.arc(width / 2, height / 2, radius, 0, 2 * Math.PI)
      ctx.fill()
      ctx.stroke()
      
      ctx.strokeStyle = Qt.rgba(interpolatedColorR, interpolatedColorG, interpolatedColorB, 1)
      
      ctx.beginPath()
      ctx.arc(width / 2, height / 2, radius, 0, percentage * 2 * Math.PI)
      ctx.fill()
      ctx.stroke()
    }
    Text {
      id: text

      font.family: "Symbols Nerd Font Mono"
      font.pointSize: 13
      font.weight: 600

      text: icon
      anchors.centerIn: parent
      color: borderColor

      states: State {
        name: "hovered"; when: canvasMouseArea.containsMouse
        PropertyChanges {
          target: text
          color: textColorHovered
        }
      }
      transitions: Transition {
        ColorAnimation {
          duration: 200
        }
      }
    }
    Timer {
      interval: 64 
      running: true
      repeat: true
      onTriggered: {
        canvas.requestPaint()
      }
    }
    Timer {
      id: canvasTimer
      interval: 16
      running: true
      repeat: true
      onTriggered: {
        if (canvasMouseArea.containsMouse && canvas.colorPhase <= 1) canvas.colorPhase += 0.08
        if (!canvasMouseArea.containsMouse && canvas.colorPhase >= 0) canvas.colorPhase -= 0.08
      }
    }
  }
  MouseArea {
    id: canvasMouseArea
    anchors.fill: canvas
    hoverEnabled: true
  }
}
