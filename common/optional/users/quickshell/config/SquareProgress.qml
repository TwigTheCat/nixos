import QtQuick
import QtQuick.Shapes

Item {
  required property real percent 
  required property string icon
  required property string backgroundColor
  required property string borderColor
  required property string borderHoveredColor
  required property string iconColor
  required property string iconHoveredColor
  
  Rectangle {
    id: backgroundRect
    width: parent.width
    height: parent.height
    color: backgroundColor // "#FF2A2A2A"

    anchors.bottom: centerRect.bottom
    // anchors.bottomMargin: -3
  }
  Rectangle {
    id: centerRect
    width: parent.width - 6
    height: parent.height
    color: backgroundColor // "#FF2A2A2A"

    anchors.horizontalCenter: parent.horizontalCenter
  }

  Rectangle {
    id: topSide
    height: 3
    width: (percent < 0.75) ? 0 : backgroundRect.width * ((percent >= 1) ? 1 : (percent - 0.75) / 0.25)
    color: borderColor
    anchors.top: backgroundRect.top
  }
  Rectangle {
    id: bottomSide
    height: 3
    width: (percent < 0.25) ? 0 : backgroundRect.width * ((percent >= 0.50) ? 1 : (percent - 0.25) / 0.25)
    color: borderColor
    anchors.bottom: backgroundRect.bottom
    anchors.right: backgroundRect.right
  }

  Rectangle {
    id: leftSide
    width: 3
    height: (percent < 0.50) ? 0 : backgroundRect.height * ((percent >= 0.75) ? 1 : (percent - 0.50) / 0.25)
    color: borderColor
    anchors.bottom: backgroundRect.bottom
  }  
  Rectangle {
    id: rightSide
    width: 3
    height: backgroundRect.height * ((percent >= 0.25) ? 1 : percent / 0.25)
    color: borderColor
    anchors.top: backgroundRect.top
    anchors.right: backgroundRect.right
  }
  
  Text {
    id: text
    font.family: "Symbols Nerd Font Mono"
    font.pointSize: 13
    font.weight: 600

    text: icon
    anchors.centerIn: backgroundRect
    color: iconColor
  }

  MouseArea {
    id: mouseArea
    anchors.fill: parent
    hoverEnabled: true
  }
  states: State {
    name: "hovered"
    when: (mouseArea.containsMouse && !modelData.active)
    PropertyChanges {
      target: rightSide
      color: borderHoveredColor
    }
    PropertyChanges {
      target: topSide
      color: borderHoveredColor
    }
    PropertyChanges {
      target: bottomSide
      color: borderHoveredColor
    }
    PropertyChanges {
      target: leftSide
      color: borderHoveredColor
    }
    PropertyChanges {
      target: text
      color: iconHoveredColor
    }
  }
  transitions: Transition {
    ColorAnimation {
      duration: 200
    }
  }
}
