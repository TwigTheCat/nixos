{
  pkgs,
  config,
  lib,
  ...
}: let
  cfg = config.twig.services.idle;
in {
  options.twig.services.idle = {
    enable = lib.mkEnableOption "wayland idle daemon";
    idleService = lib.mkOption {
      type = lib.types.enum [
        "hypridle"
        "swayidle"
      ];
    };
    locker = lib.mkOption {type = lib.types.package;};
    lockerArguments = lib.mkOption {type = lib.types.str;};
  };

  config = let
    brightnessctlNew = pkgs.brightnessctl.overrideAttrs (final: prev: {
      src = pkgs.fetchFromGitHub {
        owner = "Hummer12007";
        repo = "brightnessctl";
        rev = "e70bc55cf053caa285695ac77507e009b5508ee3";
        sha256 = "sha256-agteP/YPlTlH8RwJ9P08pwVYY+xbHApv9CpUKL4K0U0=";
      };
      configureFlags = [
        "--enable-logind"
      ];
    });
  in
    lib.mkIf cfg.enable {
      services.hypridle = lib.mkIf (cfg.idleService == "hypridle") {
        enable = true;
        settings = {
          general = {
            lock_cmd = "pidof ${cfg.locker.meta.mainProgram} || ${lib.getExe cfg.locker} ${cfg.lockerArguments}";
            before_sleep_cmd = "loginctl lock-session";
          };
          listener = [
            {
              timeout = 180;
              on-timeout = "${lib.getExe brightnessctlNew} -sc backlight set 0";
              on-resume = "${lib.getExe brightnessctlNew} -rc backlight";
            }
            {
              timeout = 180;
              on-timeout = "${lib.getExe brightnessctlNew} -sd \"*kbd_backlight\" set 0";
              on-resume = "${lib.getExe brightnessctlNew} -rd \"*kbd_backlight\"";
            }
            {
              timeout = 300;
              on-timeout = "${lib.getExe pkgs.sytemd} suspend";
            }
          ];
        };
      };

      services.swayidle = lib.mkIf (cfg.idleService == "swayidle") {
        enable = true;

        events = [
          {
            event = "lock";
            command = "pidof ${cfg.locker.meta.mainProgram} || ${lib.getExe cfg.locker} ${cfg.lockerArguments}";
          }
          {
            event = "before-sleep";
            command = "loginctl lock-session";
          }
        ];

        timeouts = [
          {
            timeout = 180;
            command = "${lib.getExe brightnessctlNew} -sc backlight set 0";
            resumeCommand = "${lib.getExe brightnessctlNew} -rc backlight";
          }
          {
            timeout = 180;
            command = "${lib.getExe brightnessctlNew} -sd \"*kbd_backlight\" set 0";
            resumeCommand = "${lib.getExe brightnessctlNew} -rd \"*kbd_backlight\"";
          }
          {
            timeout = 300;
            command = "${pkgs.systemd}/bin/systemctl suspend";
          }
        ];
      };
    };
}
