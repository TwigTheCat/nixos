{...}: {
  imports = [
    ./options.nix
    ./assertions.nix
  ];
}
