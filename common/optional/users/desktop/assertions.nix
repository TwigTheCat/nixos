{
  config,
  lib,
  ...
}: let
  cfg = config.twig.desktop;
in {
  config = {
    assertions = [
      (lib.mkIf cfg.terminal.wayland.enable {
        assertion = cfg.terminal.wayland.package != null;
        message = ''
          Option `twig.desktop.terminal.wayland.package' was set to null, but `twig.desktop.terminal.wayland.enable' is set to true.
        '';
      })
    ];
  };
}
