{
  lib,
  config,
  ...
}: let
  inherit (lib) getExe mkIf;
  inherit (lib.types) nullOr package str;

  cfg = config.twig.desktop;
in {
  options.twig.desktop = {
    enable = lib.mkEnableOption "Common settings for window managers/compositors";

    terminal = {
      wayland = {
        enable = lib.mkEnableOption "Wayland terminal common configuration";
        package = lib.mkOption {
          type = nullOr package;
          default = null;
          description = ''
            The package used for common wayland terminal configuration.
            If this option is null, then configuration is disabled.
          '';
        };
        spawnCommand = lib.mkOption {
          type = nullOr str;
          default =
            if (cfg.terminal.wayland.package != null)
            then getExe cfg.terminal.wayland.package
            else null;
          description = "The command used to spawn the terminal configured for wayland.";
        };
        commandArgs = lib.mkOption {
          type = nullOr str;
          default = null;
          description = "Arguments passed to the command when spawning the terminal configured for wayland.";
        };
      };
    };
  };

  config = lib.mkIf cfg.enable {
    wayland.windowManager = {
      hyprland = {
        settings = lib.mkIf (cfg.terminal.wayland.package != null && cfg.terminal.wayland.enable) {
          "$TERM" = "${cfg.terminal.wayland.spawnCommand}${
            if cfg.terminal.wayland.commandArgs != null
            then " ${cfg.terminal.wayland.commandArgs}"
            else ""
          }";
        };
      };

      river = {
        extraSessionVariables = mkIf (cfg.terminal.wayland.package != null && cfg.terminal.wayland.enable) {
          RIVER_TERMINAL = "${cfg.terminal.wayland.spawnCommand}${
            if cfg.terminal.wayland.commandArgs != null
            then " ${cfg.terminal.wayland.commandArgs}"
            else ""
          }";
        };
      };
    };
    programs.niri = mkIf config.programs.niri.enable {
      settings.environment = mkIf (cfg.terminal.wayland.package != null && cfg.terminal.wayland.enable) {
        NIRI_TERMINAL = "${cfg.terminal.wayland.spawnCommand}${
          if cfg.terminal.wayland.commandArgs != null
          then " ${cfg.terminal.wayland.commandArgs}"
          else ""
        }";
      };
    };
  };
}
