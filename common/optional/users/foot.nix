{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.foot;
in {
  options.twig.programs.foot = {
    enable = lib.mkEnableOption "foot";
  };

  config =
    lib.mkIf cfg.enable {
      programs.foot = {
        enable = true;
        # server.enable = true;
        settings = {
          main = let
            font-size = "14";
          in {
            # term = "xterm-256color";
            font = "${config.twig.theming.fonts.monospaceFont.name}:size=${font-size}:style=semibold:fontfeatures=liga
";
            font-italic = "${config.twig.theming.fonts.monospaceFont.name}:size=${font-size}:style=Italic:fontfeatures=liga
";
            font-bold = "${config.twig.theming.fonts.monospaceFont.name}:size=${font-size}:style=bold:fontfeatures=liga
";
            font-bold-italic = "${config.twig.theming.fonts.monospaceFont.name}:size=${font-size}:style=Bold Italic:fontfeatures=liga
";

            pad = "10x10";
          };
          csd = {
            preferred = "none";
          };
          colors = {
            alpha = "0.97";
            background = "0F1010";
            foreground = "C9C7CD";
            ## Normal/regular colors (color palette 0-7)
            regular0 = "0F1010"; # black
            regular1 = "F8C8DC"; # red
            regular2 = "BFD7B5"; # green
            regular3 = "F5BBA1"; # yellow
            regular4 = "8EB6F5"; # blue
            regular5 = "F49AC2"; # magenta
            regular6 = "C1E7E3"; # cyan
            regular7 = "E1DBEB"; # white

            ## Bright colors (color palette 8-15)
            bright0 = "2A2A2D"; # bright black
            bright1 = "F8C8DC"; # bright red
            bright2 = "BFD7B5"; # bright green
            bright3 = "F5BBA1"; # bright yellow
            bright4 = "8EB6F5"; # bright blue
            bright5 = "F49AC2"; # bright magenta
            bright6 = "C1E7E3"; # bright cyan
            bright7 = "E1DBEB"; # bright white
          };
          scrollback.multiplier = 5.0;
        };
      };
    };
}
