{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.youtube-music;
in {
  options.twig.programs.youtube-music = {
    enable = lib.mkEnableOption "youtube-music";
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      pkgs.youtube-music
    ];
  };
}
