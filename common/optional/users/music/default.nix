{...}: {
  imports = [
    ./spicetify.nix
    ./ytMusic.nix
    ./music.nix
    ./mpd.nix
  ];
}
