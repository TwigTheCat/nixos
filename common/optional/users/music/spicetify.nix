{
  inputs,
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.twig.programs.spicetify;
in {
  options.twig.programs.spicetify = {
    enable = lib.mkEnableOption "spicetify";
  };

  imports = [
    inputs.spicetify-nix.homeManagerModules.default
  ];

  config = lib.mkIf cfg.enable {
    programs.spicetify = let
      spicePkgs = inputs.spicetify-nix.legacyPackages.${pkgs.system};
    in {
      enable = true;
      enabledExtensions = builtins.attrValues {
        inherit
          (spicePkgs.extensions)
          adblock
          hidePodcasts
          seekSong
          lastfm
          beautifulLyrics
          betterGenres
          shuffle
          ; # shuffle+ (special characters are sanitized out of extension names)
      };
      enabledSnippets = [
        "removeGradient"
        "removePopular"
      ];
      alwaysEnableDevTools = true;
      windowManagerPatch = true;
      # theme = spicePkgs.themes.comfy;
      colorScheme = "custom";
      customColorScheme = {
        text = "C9C7CD";
        subtext = "C9C7CD";
        sidebar-text = "C9C7CD";
        main = "0F1010";
        sidebar = "1B1B1B";
        player = "0F1010";
        card = "0F1010";
        shadow = "0F1010";
        selected-row = "2A2A2A";
        button = "F8C8DC";
        button-active = "F49AC2";
        button-disabled = "808080";
        tab-active = "C3B1E1";
        notification = "C3B1E1";
        notification-error = "FCA5A5";
        misc = "3E3E43";
      };
    };
  };
}
