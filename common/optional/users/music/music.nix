{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.music;
in {
  options.twig.programs.music = {
    enable = lib.mkEnableOption "music apps";
  };

  config = lib.mkIf cfg.enable {
    twig.programs.spicetify.enable = lib.mkDefault true;
    twig.programs.youtube-music.enable = lib.mkDefault true;
    twig.programs.mpd.enable = lib.mkDefault true;
    twig.programs.mpc-cli.enable = lib.mkDefault true;
    twig.programs.mpdscribble.enable = lib.mkDefault true;
    twig.programs.mpd-notification.enable = lib.mkDefault true;
    twig.programs.mpdris2.enable = lib.mkDefault true;
    twig.programs.ncmpcpp.enable = lib.mkDefault true;
  };
}
