{
  pkgs,
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs;
in {
  options.twig.programs = {
    mpd.enable = lib.mkEnableOption "mpd";
    mpc-cli.enable = lib.mkEnableOption "mpc-cli";
    mpdscribble.enable = lib.mkEnableOption "mpdscribble";
    mpd-notification.enable = lib.mkEnableOption "mpd-notification";
    mpdris2.enable = lib.mkEnableOption "mpdris2";
    ncmpcpp.enable = lib.mkEnableOption "ncmpcpp";
  };
  config = {
    home.packages = [
      (lib.mkIf cfg.mpc-cli.enable pkgs.mpc-cli)
    ];

    systemd.user.services = {
      mpdscribble = lib.mkIf cfg.mpdscribble.enable {
        Unit = {
          Description = "start mpdscribble";
        };
        Install = {
          WantedBy = ["mpd.service"];
        };
        Service = {
          ExecStart = "${pkgs.mpdscribble}/bin/mpdscribble -D";
          Restart = "on-failure";
        };
      };
      mpd-notification = lib.mkIf cfg.mpd-notification.enable {
        Unit = {
          Description = "start mpd-notification";
        };
        Install = {
          WantedBy = ["mpd.service"];
        };
        Service = {
          ExecStart = "${pkgs.mpd-notification}/bin/mpd-notification";
          Restart = "on-failure";
        };
      };
    };

    services = {
      mpdris2.enable = cfg.mpdris2.enable;
      mpdris2.mpd.musicDirectory = null;
      mpd = {
        inherit (cfg.mpd) enable;
        musicDirectory = "~/Music/";
        extraConfig = ''
          audio_output {
              type "pipewire"
              name "PipeWire Sound Server"
          }
        '';
      };
    };

    programs.ncmpcpp = {
      inherit (cfg.ncmpcpp) enable;
      mpdMusicDir = "~/Music";
      package = pkgs.ncmpcpp;
      settings = {
        media_library_primary_tag = "album_artist";
        mpd_host = "192.168.1.69";
        mpd_port = 6600;
      };
    };
  };
}
