{
  config,
  lib,
  ...
}: let
  cfg = config.twig.services.mako;
in {
  options.twig.services.mako = {
    enable = lib.mkEnableOption "mako";
  };

  config = lib.mkIf cfg.enable {
    services.mako = {
      enable = true;
      defaultTimeout = 3000;
      anchor = "top-right";
      font = "${config.twig.theming.fonts.sansSerifFont.name} 12";
      backgroundColor = "#0F1010";
      textColor = "#C9C7CD";
      borderColor = "#F8C8DC";
      progressColor = "over #8EB6F5";
      extraConfig = ''
        [urgency=high]
        border-color=#F5BBA1
      '';
    };
  };
}
