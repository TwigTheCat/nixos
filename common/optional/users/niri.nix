{
  inputs,
  pkgs,
  lib,
  config,
  ...
}: let
  desktopCfg = config.twig.desktop;
  cfg = config.twig.programs.niri;
in {
  options.twig.programs.niri = {
    enable = lib.mkEnableOption "niri";
  };

  imports = [
    inputs.niri.homeModules.niri
  ];

  config = lib.mkIf cfg.enable {
    systemd.user.services.lisgd = let
      # current release of wvkbd doesn't work with niri
      # because it crashes when tapped
      # TODO: replace this when patch is released
      wvkbd = pkgs.wvkbd.overrideAttrs (final: prev: {
        src = pkgs.fetchFromGitHub {
          owner = "jjsullivan5196";
          repo = "wvkbd";
          rev = "ca53099c9de78574ac6bba6f31c72d27e1f59fca";
          hash = "sha256-Ik9RDhX+p7Q6X9loXd/pQy/eEaxWib5o4sgqQFScmy4=";
        };

        nativeBuildInputs = prev.nativeBuildInputs ++ [pkgs.scdoc];
      });
    in {
      Install = {WantedBy = [config.wayland.systemd.target];};

      Unit = {
        ConditionEnvironment = "WAYLAND_DISPLAY";
        Description = "lisgd gestures";
        BindsTo = "niri.service";
        After = "niri.service";
      };

      Service = let
        lotus-solus-screen = "'/dev/input/by-path/platform-AMDI0010:02-event'";
      in {
        Restart = "always";
        RestartSec = "10";
        ExecStart =
          lib.strings.concatMapStrings
          (x: x + " ")
          [
            (lib.getExe pkgs.lisgd)
            "-v"
            "-d"
            lotus-solus-screen
            "-g"
            "'3,DU,*,*,R,pidof wvkbd-mobintl || ${lib.getExe wvkbd} -L 275 &'"
            "-g"
            "'3,UD,*,*,R,pkill wvkbd-mobintl'"
            "-g"
            "'1,RL,T,*,R,niri msg action focus-column-right'"
            "-g"
            "'1,LR,T,*,R,niri msg action focus-column-left'"
            "-g"
            "'1,RL,B,*,R,niri msg action focus-column-right'"
            "-g"
            "'1,LR,B,*,R,niri msg action focus-column-left'"
            "-g"
            "'1,DU,L,*,R,niri msg action focus-workspace-down'"
            "-g"
            "'1,UD,L,*,R,niri msg action focus-workspace-up'"
            "-g"
            "'1,DU,R,*,R,niri msg action focus-workspace-down'"
            "-g"
            "'1,UD,R,*,R,niri msg action focus-workspace-up'"
          ];
      };
    };

    programs.niri = {
      enable = true;
      package = pkgs.niri-unstable;
      settings = {
        prefer-no-csd = true;
        screenshot-path = "~/Pictures/screenshots/%Y-%m-%d_%H-%M-%S.png";
        hotkey-overlay.skip-at-startup = true;

        spawn-at-startup = [
          {command = ["quickshell"];}
        ];

        outputs.eDP-1 = {
          position = {
            x = 0;
            y = 0;
          };
        };

        input = {
          focus-follows-mouse = {
            enable = true;
            max-scroll-amount = "50%";
          };

          touchpad = {
            accel-profile = "adaptive";
            accel-speed = 0.65;
          };

          tablet.map-to-output = "eDP-1";
          touch.map-to-output = "eDP-1";

          keyboard = {
            repeat-delay = 300;
            repeat-rate = 50;
          };
        };

        switch-events = let
          locker = [(lib.getExe config.twig.services.idle.locker) config.twig.services.idle.lockerArguments];
        in {
          lid-close.action.spawn = locker;
          lid-open.action.spawn = locker;
        };

        workspaces = {
          "1" = {
            open-on-output = "HDMI-A-1";
          };
          "2" = {
            open-on-output = "eDP-1";
          };
          "3" = {
            open-on-output = "HDMI-A-1";
          };
          "4" = {
            open-on-output = "HDMI-A-1";
          };
          "5" = {
            open-on-output = "eDP-1";
          };
          "6" = {
            open-on-output = "eDP-1";
          };
          "7" = {
            open-on-output = "HDMI-A-1";
          };
        };

        layout = {
          preset-column-widths = [
            {proportion = 1.0;}
            {proportion = 0.5;}
          ];
          gaps = 5;
          insert-hint = {
            enable = true;
            display = {color = "#F8C8DC80";};
          };
          focus-ring = {
            enable = false;
          };
          border = {
            enable = true;
            width = 2;
            active = {color = "#F8C8DC";};
            inactive = {color = "#2A2A2D";};
          };
          default-column-width = {proportion = 1.0;};
        };
        window-rules = [
          {
            default-window-height = {proportion = 1.0;};
          }
        ];

        binds = let
          # This fixes the anchoring for tofi
          # TODO: remove this when its fixed in main
          tofiOverride = pkgs.tofi.overrideAttrs (final: prev: {
            src = pkgs.fetchFromGitHub {
              owner = "itshog";
              repo = "tofi";
              rev = "1aa56b12c60cb42b00d85808fc21b343a4a4682c";
              hash = "sha256-KiSkb8HOzBnPyzQcHTyUmVixwpls3/o9BbDBkNWu71c=";
            };
          });
          tofiCmd = [
            "${lib.getExe' tofiOverride "tofi-drun"}"
            "--font"
            "${config.twig.theming.fonts.monospaceFont.package}/share/fonts/truetype/NerdFonts/CaskaydiaCove/CaskaydiaCoveNerdFontMono-Regular.ttf"
            "--hint-font"
            "false"
            "--prompt"
            ""
            "--prompt-padding=0"
          ];
          inherit
            (config.lib.niri.actions)
            focus-workspace-up
            focus-column-left
            focus-column-right
            focus-workspace-down
            focus-workspace
            move-window-to-workspace
            close-window
            fullscreen-window
            screenshot
            consume-or-expel-window-right
            consume-or-expel-window-left
            switch-preset-column-width
            ;
        in {
          "Super+Shift+Return".action.spawn = ["$NIRI_TERM"];

          "Super+r".action.spawn = tofiCmd;

          "Ctrl+Shift+q".action = close-window {};
          "Super+f".action = fullscreen-window {};

          "Super+g".action = screenshot {};

          "Super+Up".action = focus-workspace-up {};
          "Super+Down".action = focus-workspace-down {};
          "Super+Right".action = focus-column-right {};
          "Super+Left".action = focus-column-left {};

          "Super+Shift+Right".action = consume-or-expel-window-right {};
          "Super+Shift+Left".action = consume-or-expel-window-left {};

          "Super+t".action = switch-preset-column-width {};

          "Super+1".action = focus-workspace "1";
          "Super+2".action = focus-workspace "2";
          "Super+3".action = focus-workspace "3";
          "Super+4".action = focus-workspace "4";
          "Super+q".action = focus-workspace "5";
          "Super+w".action = focus-workspace "6";
          "Super+e".action = focus-workspace "7";

          "Super+Shift+1".action = move-window-to-workspace "1";
          "Super+Shift+2".action = move-window-to-workspace "2";
          "Super+Shift+3".action = move-window-to-workspace "3";
          "Super+Shift+4".action = move-window-to-workspace "4";
          "Super+Shift+q".action = move-window-to-workspace "5";
          "Super+Shift+w".action = move-window-to-workspace "6";
          "Super+Shift+e".action = move-window-to-workspace "7";

          "XF86MonBrightnessUp".action.spawn = [(lib.getExe pkgs.brightnessctl) "set" "10%+"];
          "XF86MonBrightnessDown".action.spawn = [(lib.getExe pkgs.brightnessctl) "set" "10%-"];

          "XF86AudioPlay".action.spawn = [(lib.getExe pkgs.playerctl) "play-pause"];
          "XF86AudioPrev".action.spawn = [(lib.getExe pkgs.playerctl) "previous"];
          "XF86AudioNext".action.spawn = [(lib.getExe pkgs.playerctl) "next"];

          "XF86AudioRaiseVolume".action.spawn = [(lib.getExe pkgs.pamixer) "--increase" "5"];
          "XF86AudioLowerVolume".action.spawn = [(lib.getExe pkgs.pamixer) "--decrease" "5"];
          "XF86AudioMute".action.spawn = [(lib.getExe pkgs.pamixer) "--toggle-mute"];
        };
      };
    };
  };
}
