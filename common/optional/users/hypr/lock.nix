{
  inputs,
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.hyprlock;
in {
  options.twig.programs.hyprlock = {
    enable = lib.mkEnableOption "hyprlock";
  };

  config = lib.mkIf cfg.enable {
    programs.hyprlock = {
      enable = true;
      # package = inputs.hyprlock.packages.${pkgs.system}.hyprlock;
      settings = {
        general = {
          no_fade_in = true;
          # no_fade_out = true;
          immediate_render = true;
        };

        label = {
          monitor = "";
          text = "$TIME12";
          text_align = "center";
          color = "rgb(C9C7CD)";
          font_size = 27;
          font_family = config.twig.theming.fonts.monospaceFont.name;
          rotate = 0;

          position = "0, -50";
          halign = "center";
          valign = "top";
        };

        background = {
          monitor = "";
          path = "${../wallpapers/spookys-mansion.png}";
          color = "rgb(0F1010)";
        };

        input-field = {
          hide_input = false;
          fade_on_empty = false;
          dots_center = true;
          swap_font_color = true;
          monitor = "";
          size = "200, 50";
          outline_thickness = 3;
          outer_color = "rgb(F8C8DC)";
          inner_color = "rgb(2A2A2A)";
          check_color = "rgb(F5BBA1)";
          fail_color = "rgb(FCA5A5)";
          font_color = "rgb(C9C7CD)";
          placeholder_text = "<i>Password: </i>";
          position = "0, 50";
          halign = "center";
          valign = "bottom";
        };
      };
    };
  };
}
