{
  pkgs,
  config,
  lib,
  inputs,
  ...
}: let
  desktopCfg = config.twig.desktop;
  cfg = config.twig.programs.hyprland;
in {
  options.twig.programs.hyprland = {
    enable = lib.mkEnableOption "hyprland";
  };

  config = lib.mkIf cfg.enable {
    wayland.windowManager.hyprland = {
      enable = true;
      systemd.enable = true;
      # package = inputs.hyprland.packages.${pkgs.system}.hyprland;
      plugins = [
        pkgs.hyprlandPlugins.hyprgrass
        # inputs.hyprgrass.packages.${pkgs.system}.default
        # inputs.hy3.packages.${pkgs.system}.hy3
        # inputs.hyprsplit.packages.${pkgs.system}.hyprsplit
      ];
      settings = {
        # "$EDITOR" = "${pkgs.emacs}/bin/emacsclient -c -a emacs";

        "$red" = "F8C8DC";
        "$magenta" = "F49AC2";
        "$green" = "BFD7B5";
        "$black" = "0F1010";
        "$light_black" = "2A2A2D";
        "$yellow" = "F5BBA1";
        "$blue" = "8EB6F5";
        "$cyan" = "C1E7E3";
        "$white" = "E1DBEB";
        "$purple" = "C3B1E1";

        env = [
          "TERM, $TERM"
          # "EDITOR, $EDITOR"
        ];

        debug.disable_logs = false;

        monitor = [
          "eDP-1,1920x1200@60,0x0,1, transform, 0"
          # "HDMI-A-1,preferred,1920x0,1"
        ];

        windowrulev2 = [
          # "center, floating:1"
        ];

        exec = [
        ];
        exec-once = let
          hyprctlMonCmd = "hyprctl keyword monitor eDP-1,preferred,auto,1,transform,";
          hyprctlTabCmd = "hyprctl keyword input:tablet:transform ";
          hyprctlTchCmd = "hyprctl keyword input:touchdevice:transform ";
          autoRotateScript =
            pkgs.writeShellScriptBin "autoRotateScreen"
            ''
              ${lib.getExe' pkgs.iio-sensor-proxy "monitor-sensor"} --accel | while IFS= read -r line; do
                if (echo $line | grep -q "normal"); then
                  ${hyprctlMonCmd}0;
                  ${hyprctlTabCmd}0;
                  ${hyprctlTchCmd}0;
                elif (echo $line | grep -q "right-up"); then
                  ${hyprctlMonCmd}3;
                  ${hyprctlTabCmd}3;
                  ${hyprctlTchCmd}3;
                elif (echo $line | grep -q "left-up"); then
                  ${hyprctlMonCmd}1;
                  ${hyprctlTabCmd}1;
                  ${hyprctlTchCmd}1;
                elif (echo $line | grep -q "bottom-up"); then
                  ${hyprctlMonCmd}2;
                  ${hyprctlTabCmd}2;
                  ${hyprctlTchCmd}2;
                fi
              done
            '';
          autoRotateCmd = lib.getExe autoRotateScript;
        in [
          "${autoRotateCmd}"
          "${lib.getExe pkgs.mako}"
        ];

        plugin = {
          touch_gestures = {
            sensitivity = 1.0;
            workspace_swipe_fingers = 3;
            resize_on_border_long_press = true;
            edge_margin = 20;
            # emulate_touchpad_swipe = true;
            # workspace_swipe_edge = "l";

            hyprgrass-bind = [
              ", swipe:3:r, workspace, +1"
              ", swipe:3:l, workspace, -1"
              ", swipe:3:u, exec, pkill -9 wvkbd; ${lib.getExe pkgs.wvkbd} -L 300"
              ", swipe:3:d, exec, pkill -9 wvkbd"
            ];
          };
          #   "hy3" = {
          #     "tab_first_window" = true;
          #     "tabs" = {
          #       "height" = 5;
          #       "adding" = 5;
          #       "render_text" = false;
          #       "col.active" = "rgba($redff)";
          #       "col.urgent" = "rgba($yellowff)";
          #       "col.inactive" = "rgba(9998A8ff)";
          #     };
          #     "autotile.enable" = true;
          #   };
          # "hpyrsplit" = {
          # num_workspaces = 4;
          # };
        };

        workspace = [
          "1, monitor:HDMI-A-1"
          "2, monitor:eDP-1"
          "3, monitor:HDMI-A-1"
          "4, monitor:HDMI-A-1"
          "5, monitor:eDP-1"
          "6, monitor:eDP-1"
          "7, monitor:HDMI-A-1"
        ];

        general = {
          gaps_in = 4;
          gaps_out = 2;
          border_size = 2;
          "col.active_border" = "rgb($red)"; #rgb($magenta) rgb($yellow) rgb($green) rgb($blue) rgb($cyan) rgb($purple) 45deg";
          "col.inactive_border" = "rgb($light_black)"; # rgb($white)";
          # layout = "hy3";
          layout = "dwindle";
        };

        device = [
          {
            name = "synac780:00-06cb:cf00-touchpad";
            sensitivity = 0.5;
          }
        ];

        decoration = {
          rounding = 0;

          blur = {
            enabled = false;
            passes = 4;
          };

          active_opacity = 0.99;
          inactive_opacity = 0.97;

          shadow = {
            enabled = false;
            range = 15;
            render_power = 3;
            color = "rgba($black50)";
          };
        };

        animations = {
          enabled = true;
          bezier = [
            "myBezier, 0.05, 0.9,  0.1, 1.0"
            "linear,   1,    1,    1,   1"
            "wind,     0.05, 0.9,  0.1, 1.0"
            "winIn,    0.1,  1.1,  0.1, 1.1"
            "winOut,   0.3,  -0.3, 0,   1"
            "overshot, 0.05, 0.9,  0.05, 1.2"
          ];

          animation = [
            "windows    , 1, 4   , wind"
            "windowsIn  , 1, 3   , winIn, slide"
            "windowsOut , 1, 4   , winOut"
            "windowsMove, 1, 3   , wind"
            # "border     , 1, 10  , default"
            # "borderangle, 1, 30  , linear, loop"
            "fade       , 1, 1   , default"
            "workspaces , 1, 3   , wind"
          ];
        };

        dwindle = {
          pseudotile = true;
          preserve_split = true;
          force_split = 2;
          special_scale_factor = 0.75;
        };

        misc = {
          disable_hyprland_logo = true;
          vfr = true;
        };

        opengl = {
          force_introspection = 1;
        };

        input = {
          kb_layout = "us";
          kb_options = "caps:escape, compose:menu";
          numlock_by_default = true;

          repeat_rate = 50;
          repeat_delay = 300;

          follow_mouse = 1;

          touchpad = {
            natural_scroll = true;
            disable_while_typing = true;

            middle_button_emulation = true;
          };
          tablet = {
            output = "eDP-1";
          };
          touchdevice = {
            output = "eDP-1";
          };

          sensitivity = 0;
        };

        gestures = {
          workspace_swipe = true;
          workspace_swipe_touch = true;
          workspace_swipe_cancel_ratio = 0.15;
        };

        "$mainMod" = "SUPER";

        bind = let
          tofiCmd = "${lib.getExe' pkgs.tofi "tofi-drun"} --font ${config.twig.theming.fonts.monospaceFont.package}/share/fonts/truetype/NerdFonts/CaskaydiaCove/CaskaydiaCoveNerdFontMono-Regular.ttf --hint-font false --prompt '' --prompt-padding 0";
        in [
          ###### Launching stuff / Scripts

          "$mainMod, R, exec, ${tofiCmd}"

          "SUPER, p, exec, ${pkgs.hyprpicker}/bin/hyprpicker -a -f hex"

          "SUPER_SHIFT, return, exec, $TERM"
          "SUPER_SHIFT, n, exec, $EDITOR"

          "SUPER, g, exec, ${pkgs.grimblast}/bin/grimblast save area - | ${pkgs.wl-clipboard}/bin/wl-copy"
          "SUPER_SHIFT, g, exec, ${pkgs.grimblast}/bin/grimblast save area - | ${pkgs.satty}/bin/satty -f -"

          ###### Brightness Control

          ", XF86MonBrightnessUp, exec, ${pkgs.brightnessctl}/bin/brightnessctl set 10%+"
          ", XF86MonBrightnessDown, exec, ${pkgs.brightnessctl}/bin/brightnessctl set 10%-"

          ###### Audio Control

          ", XF86AudioPlay, exec, ${pkgs.playerctl}/bin/playerctl play-pause"
          ", XF86AudioPrev, exec, ${pkgs.playerctl}/bin/playerctl previous"
          ", XF86AudioNext, exec, ${pkgs.playerctl}/bin/playerctl next"

          ", XF86AudioLowerVolume, exec, ${pkgs.pamixer}/bin/pamixer --decrease 5"
          ", XF86AudioRaiseVolume, exec, ${pkgs.pamixer}/bin/pamixer --increase 5"
          ", XF86AudioMute, exec, ${pkgs.pamixer}/bin/pamixer --toggle-mute"

          # "SUPER, mouse:276, exec, ~/.config/hypr/scripts/mute_mic.sh"
          # "SUPER, m, exec, ~/.config/hypr/scripts/mute_mic.sh"

          ###### Window and Workspace Manipulation

          "CTRL_SHIFT, Q, killactive,"

          "$mainMod, Space, togglefloating,"
          "$mainMod, F, fullscreen,"

          "$mainMod, S, togglespecialworkspace,"
          "SUPER_SHIFT, S, movetoworkspace, special"

          "$mainMod, 1, workspace, 1"
          "$mainMod, 2, workspace, 2"
          "$mainMod, 3, workspace, 3"
          "$mainMod, 4, workspace, 4"
          "$mainMod, Q, workspace, 5"
          "$mainMod, W, workspace, 6"
          "$mainMod, E, workspace, 7"

          "$mainMod SHIFT, 1, movetoworkspace, 1"
          "$mainMod SHIFT, 2, movetoworkspace, 2"
          "$mainMod SHIFT, 3, movetoworkspace, 3"
          "$mainMod SHIFT, 4, movetoworkspace, 4"
          "$mainMod SHIFT, Q, movetoworkspace, 5"
          "$mainMod SHIFT, W, movetoworkspace, 6"
          "$mainMod SHIFT, E, movetoworkspace, 7"

          "$mainMod SHIFT, l, workspace, e+1"
          "$mainMod SHIFT, h, workspace, e-1"
          "$mainMod SHIFT, right, workspace, e+1"
          "$mainMod SHIFT, left, workspace, e-1"

          # "$mainMod, h, hy3:focustab, left, wrap"
          # "$mainMod, l, hy3:focustab, right, wrap"
          # "$mainMod, left, hy3:focustab, left, wrap"
          # "$mainMod, right, hy3:focustab, right, wrap"

          # "$mainMod, v, hy3:makegroup, h"
          # "$mainMod, t, hy3:makegroup, tab"

          ######
        ];
        bindm = [
          # Resizing and Moving Windows
          "$mainMod, mouse:272, movewindow"
          "$mainMod, mouse:273, resizewindow"
        ];
        bindn = [
          # Switch Tabs by Clicking
          # ", mouse:272, hy3:focustab, mouse"
        ];
        bindl = [
          # Lock on Lid Close
          ",switch:on:Lid Switch, exec, ${lib.getExe config.twig.services.idle.locker} ${config.twig.services.idle.lockerArguments}"
          ",switch:off:Lid Switch, exec, ${lib.getExe config.twig.services.idle.locker} ${config.twig.services.idle.lockerArguments}"
        ];
      };
    };
  };
}
