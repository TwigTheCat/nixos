{
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.pay-respects;
in {
  options.twig.programs.pay-respects = {
    enable = lib.mkEnableOption "pay-respects";
    fishIntegration = lib.mkEnableOption "Fish integration for pay-respects";
  };

  config = lib.mkIf cfg.enable {
    programs.pay-respects = {
      enable = true;
      enableFishIntegration = cfg.fishIntegration;
    };
    home.packages = [pkgs.nix-index];
  };
}
