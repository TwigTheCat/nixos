{...}: {
  imports = [
    ./zoxide.nix
    ./fzf.nix
    ./fd.nix
    ./jq.nix
    ./bat.nix
    ./eza.nix
    ./cli.nix
    ./btop.nix
    ./bottom.nix
    ./ripgrep.nix
    ./zellij.nix
    ./direnv.nix
    ./carapace.nix
    ./pay-respects.nix
  ];
}
