{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.jq;
in {
  options.twig.programs.jq = {
    enable = lib.mkEnableOption "jq";
  };

  config = lib.mkIf cfg.enable {
    programs.jq = {
      enable = true;
    };
  };
}
