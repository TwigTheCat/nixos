{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.bottom;
in {
  options.twig.programs.bottom = {
    enable = lib.mkEnableOption "bottom";
  };

  config = lib.mkIf cfg.enable {
    programs.bottom = {
      enable = true;
    };
  };
}
