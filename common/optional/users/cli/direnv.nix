{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.direnv;
in {
  options.twig.programs.direnv = {
    enable = lib.mkEnableOption "direnv";
  };

  config = lib.mkIf cfg.enable {
    programs.direnv = {
      enable = true;
      nix-direnv.enable = true;
    };
  };
}
