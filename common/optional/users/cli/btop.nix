{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.btop;
in {
  options.twig.programs.btop = {
    enable = lib.mkEnableOption "btop";
  };

  config = lib.mkIf cfg.enable {
    programs.btop = {
      enable = true;
    };
  };
}
