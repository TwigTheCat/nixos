{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.bat;
in {
  options.twig.programs.bat = {
    enable = lib.mkEnableOption "bat";
  };

  #TODO: check other configuration options for bat
  config = lib.mkIf cfg.enable {
    programs.bat = {
      enable = true;
    };
  };
}
