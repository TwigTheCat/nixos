{
  pkgs,
  lib,
  config,
  ...
}: let
  cfg = config.twig.programs.carapace;
in {
  options.twig.programs.carapace = {
    enable = lib.mkEnableOption "carapace";
  };

  config = lib.mkIf cfg.enable {
    programs.carapace = {
      enable = true;
    };
  };
}
