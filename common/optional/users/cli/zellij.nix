{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.zellij;
in {
  options.twig.programs.zellij = {
    enable = lib.mkEnableOption "zellij";
  };

  config = lib.mkIf cfg.enable {
    programs.zellij.enable = true;
  };
}
