{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.ripgrep;
in {
  options.twig.programs.ripgrep = {
    enable = lib.mkEnableOption "ripgrep";
  };

  config = lib.mkIf cfg.enable {
    programs.ripgrep = {
      enable = true;
    };
  };
}
