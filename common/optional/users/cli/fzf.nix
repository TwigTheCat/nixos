{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.fzf;
in {
  options.twig.programs.fzf = {
    enable = lib.mkEnableOption "fzf";
  };

  #TODO: look at other options for fzf
  config = lib.mkIf cfg.enable {
    programs.fzf = {
      enable = true;
    };
  };
}
