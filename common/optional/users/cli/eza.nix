{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.eza;
in {
  options.twig.programs.eza = {
    enable = lib.mkEnableOption "eza";
  };

  config = lib.mkIf cfg.enable {
    programs.eza = {
      enable = true;
    };
  };
}
