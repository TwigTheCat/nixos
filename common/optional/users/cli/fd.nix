{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.fd;
in {
  options.twig.programs.fd = {
    enable = lib.mkEnableOption "fd";
  };

  config = lib.mkIf cfg.enable {
    programs.fd = {
      enable = true;
    };
  };
}
