{
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.zoxide;
in {
  options.twig.programs.zoxide = {
    enable = lib.mkEnableOption "zoxide";
    fish.enable = lib.mkEnableOption "fish integration";
    zsh.enable = lib.mkEnableOption "zsh integration";
  };

  config = lib.mkIf cfg.enable {
    programs = {
      zoxide = {
        enable = true;
        options = ["--cmd cd"];
        enableZshIntegration = cfg.zsh.enable;
        enableFishIntegration = cfg.fish.enable;
      };
    };
  };
}
