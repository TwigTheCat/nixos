{
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.cli;
  mkAppOption = name:
    lib.mkOption {
      type = lib.types.bool;
      default = cfg.enable;
      description = "install ${name} (enabled by default)";
    };
in {
  options.twig.programs.cli = {
    enable = lib.mkEnableOption "cli applications";

    bc.enable = mkAppOption "bc";
    p7zip.enable = mkAppOption "p7zip";
    brightnessctl.enable = mkAppOption "brightnessctl";
    libnotify.enable = mkAppOption "libnotify";
    microfetch.enable = mkAppOption "microfetch";
    appimage-run.enable = mkAppOption "appimage-run";
    dotool.enable = mkAppOption "dotool";
  };

  config = lib.mkIf cfg.enable {
    twig.programs = {
      zoxide.enable = lib.mkDefault true;
      ripgrep.enable = lib.mkDefault true;
      jq.enable = lib.mkDefault true;
      fd.enable = lib.mkDefault true;
      bat.enable = lib.mkDefault true;
      btop.enable = lib.mkDefault true;
      fzf.enable = lib.mkDefault true;
      eza.enable = lib.mkDefault true;
      direnv.enable = lib.mkDefault true;
      zellij.enable = lib.mkDefault true;
      carapace.enable = lib.mkDefault true;
    };
    home.packages = let
      brightnessctlNew = pkgs.brightnessctl.overrideAttrs (final: prev: {
        src = pkgs.fetchFromGitHub {
          owner = "Hummer12007";
          repo = "brightnessctl";
          rev = "e70bc55cf053caa285695ac77507e009b5508ee3";
          sha256 = "sha256-agteP/YPlTlH8RwJ9P08pwVYY+xbHApv9CpUKL4K0U0=";
        };
        configureFlags = [
          "--enable-logind"
        ];
      });
    in [
      (lib.mkIf cfg.bc.enable pkgs.bc)
      (lib.mkIf cfg.p7zip.enable pkgs.p7zip)
      (lib.mkIf cfg.brightnessctl.enable brightnessctlNew)
      (lib.mkIf cfg.libnotify.enable pkgs.libnotify)
      (lib.mkIf cfg.microfetch.enable pkgs.microfetch)
      (lib.mkIf cfg.appimage-run.enable pkgs.appimage-run)
      (lib.mkIf cfg.dotool.enable pkgs.dotool)
    ];
  };
}
