{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.zsh;
in {
  options.twig.programs.zsh = {
    enable = lib.mkEnableOption "zsh";
    carapace.enable = lib.mkEnableOption "carapace zsh integration";
  };
  config = lib.mkIf cfg.enable {
    home.packages = [
      pkgs.nix-zsh-completions
    ];
    programs.carapace = {
      enableZshIntegration = cfg.carapace.enable;
    };
    programs.zsh = {
      enable = true;
      autocd = true;
      enableCompletion = true;
      zprof.enable = false;
      autosuggestion.enable = true;
      syntaxHighlighting.enable = false;
      sessionVariables = {
        ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE = "fg=#3E3E43,bg=black,bold,underline";
        EDITOR = "nvim";
        FZF_DEFAULT_OPTS =
          "$FZF_DEFAULT_OPTS"
          + ''
            --color=fg:#C9C7CD,bg:#0F1010,hl:#8EB6F5
            --color=fg+:#C9C7CD,bg+:#1B1B1B,hl+:#8EC4E5
            --color=info:#BFD7B5,prompt:#F8C8DC,pointer:#F8C8DC
            --color=marker:#BEE7c5,spinner:#C3B1E1,header:#C1E7E3'';
      };
      plugins = [
        {
          name = "zsh-defer";
          src = pkgs.fetchFromGitHub {
            owner = "romkatv";
            repo = "zsh-defer";
            rev = "53a26e287fbbe2dcebb3aa1801546c6de32416fa";
            sha256 = "sha256-MFlvAnPCknSgkW3RFA8pfxMZZS/JbyF3aMsJj9uHHVU=";
          };
        }
        {
          name = "fast-syntax-highlighting";
          src = pkgs.fetchFromGitHub {
            owner = "zdharma-continuum";
            repo = "fast-syntax-highlighting";
            rev = "cf318e06a9b7c9f2219d78f41b46fa6e06011fd9";
            sha256 = "sha256-RVX9ZSzjBW3LpFs2W86lKI6vtcvDWP6EPxzeTcRZua4=";
          };
        }
        {
          name = "fzf-tab";
          src = pkgs.fetchFromGitHub {
            owner = "Aloxaf";
            repo = "fzf-tab";
            rev = "6aced3f35def61c5edf9d790e945e8bb4fe7b305";
            sha256 = "sha256-EWMeslDgs/DWVaDdI9oAS46hfZtp4LHTRY8TclKTNK8=";
          };
        }
        {
          name = "zsh-256color";
          src = pkgs.fetchFromGitHub {
            owner = "chrissicool";
            repo = "zsh-256color";
            rev = "559fee48bb74b75cec8b9887f8f3e046f01d5d8f";
            sha256 = "sha256-P/pbpDJmsMSZkNi5GjVTDy7R+OxaIVZhb/bEnYQlaLo=";
          };
        }
        {
          name = "zsh-autopair";
          src = pkgs.fetchFromGitHub {
            owner = "hlissner";
            repo = "zsh-autopair";
            rev = "449a7c3d095bc8f3d78cf37b9549f8bb4c383f3d";
            sha256 = "sha256-3zvOgIi+q7+sTXrT+r/4v98qjeiEL4Wh64rxBYnwJvQ=";
          };
        }
      ];
      shellAliases = {
        ls = "eza -1 --icons --git --group-directories-first";
        tree = "eza --tree --icons";
        grep = "rg";
      };
      history = {
        expireDuplicatesFirst = true;
        size = 2000;
        save = 2000;
        append = true;
      };
      initExtra = ''
        zstyle ':completion:*' list-colors ''${(s.:.)LS_COLORS}
        zstyle ':fzf-tab:*' prefix '·'
        zstyle ':fzf-tab:complete:cd:*' fzf-preview 'eza -1 --color=always $realpath'
        zstyle ':fzf-tab:*' use-fzf-default-opts yes
        # zstyle ':completion:*' matcher-list ''' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

        setopt PROMPT_SUBST
        # setopt glob_dots

        autoload -Uz vcs_info

        zstyle ':vcs_info:*' stagedstr '%B%F{green}'
        zstyle ':vcs_info:*' unstagedstr '%B%F{yellow}'
        zstyle ':vcs_info:*' check-for-changes true
        zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{11}%r'
        zstyle ':vcs_info:*' enable git svn
        theme_precmd () {
          if [[ -z $(git ls-files --other --exclude-standard 2> /dev/null) ]] {
              zstyle ':vcs_info:*' formats ' %B%F{yellow}%u%c(%b)'
          } else {
              zstyle ':vcs_info:*' formats ' %B%F{yellow}%u%c(%b)'
          }

          vcs_info
        }

        # PROMPT='%B%F{magenta}[%c''${vcs_info_msg_0_}%B%F{magenta}%{$reset_color%}] '

        autoload -U add-zsh-hook
        add-zsh-hook precmd theme_precmd

        path+=(~/go/bin/)
        path+=(~/.cargo/bin/)
        path+=(~/.local/bin/)
        export PATH

        function in_nix_shell() {
          if [ ! -z ''${IN_NIX_SHELL+x} ];
            then echo "%F{blue} %f";
          else
            echo "%F{red}ζ%f";
          fi
        }

        export NEWLINE=$'\n'
        function prompt() {
          # local gitprompt = "%F{magenta}[''${vcs_info_msg_0_}%F{magenta}%{$reset_color%}]"
          PROMPT="%F{yellow}%n%f%F{green}@%f%F{blue}%M%f%F{green}:%f%F{magenta}%~%f''${NEWLINE}$(in_nix_shell)%b "

          if [[ $? -ne 130 ]]; then
            RPS1='%B%F{magenta}%(?..[%?] )%f'
          else
            RPS1=""
          fi
        }
        precmd_functions+=(prompt)

        bindkey '\e[A' history-search-backward
        bindkey '\e[B' history-search-forward
        bindkey "^[[1;5C" forward-word
        bindkey "^[[1;5D" backward-word
      '';
    };
  };
}
