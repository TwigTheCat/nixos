{...}: {
  imports = [
    ./nu.nix
    ./zsh.nix
    ./fish.nix
  ];
}
