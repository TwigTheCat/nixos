{
  pkgs,
  config,
  lib,
  ...
}: let
  cfg = config.twig.programs.fish;
in {
  options.twig.programs.fish = {
    enable = lib.mkEnableOption "fish";
    carapace.enable = lib.mkEnableOption "carapace fish integration";
  };
  config = lib.mkIf cfg.enable {
    programs.carapace = {
      enableFishIntegration = cfg.carapace.enable;
    };
    programs.fish = {
      enable = true;
      interactiveShellInit = ''
        # tide configure --auto --style=Lean --prompt_colors='16 colors' --show_time='12-hour format' --lean_prompt_height='Two lines' --prompt_connection=Disconnected --prompt_spacing=Compact --icons='Few icons' --transient=No
      '';
      plugins = [
        {
          inherit (pkgs.fishPlugins.fzf) src name;
        }
        # {
        #   name = "tide";
        #   inherit (pkgs.fishPlugins.tide) src;
        # }
      ];
      shellAliases = {
        ls = "eza -1";
      };
      shellInit = ''
        set fish_greeting

        set FLAKE ~/nixos
        set EDITOR nvim

        set -U FZF_COMPLETE 2
        set FZF_DEFAULT_COMMAND 'fd --type file --color=always'
        set FZF_CTRL_T_COMMAND '$FZF_DEFAULT_COMMAND'
        set FZF_PREVIEW_FILE_CMD 'bat --theme=base16 --color=always --style=grid --line-range :300'

        set FZF_DEFAULT_OPTS "\
        --height 40% --layout=reverse --ansi \
        --color=fg:#C9C7CD,bg:#0F1010,hl:#8EB6F5 \
        --color=info:#BFD7B5,prompt:#F8C8DC,pointer:#F8C8DC \
        --color=marker:#BEE7c5,spinner:#C3B1E1,header:#C1E7E3"
      '';
    };
  };
}
