{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.programs.nushell;
in {
  options.twig.programs.nushell = {
    enable = lib.mkEnableOption "nushell";
    carapace.enable = lib.mkEnableOption "carapace nushell integration";
  };

  config = lib.mkIf cfg.enable {
    programs.carapace = {
      enableNushellIntegration = cfg.carapace.enable;
    };
    programs.nushell = {
      enable = true;
      extraConfig = ''
        $env.config.show_banner = false
        ${
          if cfg.carapace.enable
          then ''
            source ~/.cache/carapace/init.nu
            let carapace_completer = {|spans|
              ${lib.getExe pkgs.carapace} $spans.0 nushell ...$spans | from json
            }

            $env.config.completions.external = {
                enable: true
                max_results: 100
                completer: $carapace_completer
            }
          ''
          else ''
            let fish_completer = {|spans|
                ${lib.getExe pkgs.fish} --command $'complete "--do-complete=($spans | str join " ")"'
                | from tsv --flexible --noheaders --no-infer
                | rename value description
            }
          ''
        }
        $env.config.completions.case_sensitive = false;
        $env.config.completions.sort = "smart";
        $env.config = {
          show_banner: false,
        };
        $env.LS_COLORS = ""
        $env.PROMPT_COMMAND = { $"(ansi yellow)($env.USER) (ansi green)at (ansi blue)(hostname) (ansi green)in (ansi magenta)(pwd | str replace -r $"($env.HOME)" '~' )(ansi reset) \n\n" }
        $env.PROMPT_INDICATOR = "ζ "
        $env.PATH = ($env.PATH | split row (char esep) | append "~/.local/bin")
      '';
      extraEnv = lib.mkIf cfg.carapace.enable ''
        mkdir ~/.cache/carapace
        ${lib.getExe pkgs.carapace} _carapace nushell | save --force ~/.cache/carapace/init.nu
      '';
      shellAliases = {
        grep = "rg";
        tree = "eza --tree --icons";
      };
      environmentVariables = {
        PAGER = "'${lib.getExe pkgs.most}'";
        EDITOR = "'nvim'";
        FLAKE = ''$"($env.HOME)/nixos"'';
      };
    };
  };
}
