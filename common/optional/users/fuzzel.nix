{
  config,
  pkgs,
  lib,
  ...
}: let
  cfg = config.twig.programs.fuzzel;
in {
  options.twig.programs.fuzzel = {
    enable = lib.mkEnableOption "fuzzel";
  };

  config = lib.mkIf cfg.enable {
    home.packages = [pkgs.papirus-icon-theme]; # needed for icons to work
    programs.fuzzel = {
      enable = true;
      settings = {
        main = {
          font = "${config.twig.theming.fonts.monospaceFont.name}:size=11:style=medium";
          prompt = "'meow '";
          icon-theme = "Papirus-Dark";
          terminal = "foo";
          lines = 20;
          width = 30;
          tabs = 4;
          horizontal-pad = 25;
          vertical-pad = 15;
          inner-pad = 10;
          line-height = 17;
          exit-on-keyboard-focus-loss = false;
          dpi-aware = "no";
        };
        colors = {
          background = "0F1010ff";
          text = "C9C7CDff";
          prompt = "C9C7CDff";
          input = "C9C7CDff";
          match = "F8C8DCff";
          selection = "3E3E43ff";
          selection-text = "F39AC2ff";
          selection-match = "F8C8DCff";
          border = "F8C8DCff";
        };
        border = {
          width = 3;
          radius = 0;
        };
      };
    };
  };
}
