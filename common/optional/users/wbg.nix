{
  inputs,
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.twig.services.wbg;
in {
  options.twig.services.wbg = {
    enable = lib.mkEnableOption "wbg";
    bg-path = lib.mkOption {
      type = lib.types.str;
      default = "";
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.user.services.wbg = {
      Install = {WantedBy = [config.wayland.systemd.target];};

      Unit = {
        ConditionEnvironment = "WAYLAND_DISPLAY";
        Description = "wbg";
        After = [config.wayland.systemd.target];
        PartOf = [config.wayland.systemd.target];
      };

      Service = {
        ExecStart = "${lib.getExe inputs.nixpkgs-wayland.packages.${pkgs.system}.wbg} -s ${cfg.bg-path}";
        Restart = "always";
        RestartSec = "10";
      };
    };
  };
}
