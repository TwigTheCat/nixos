{
  pkgs,
  config,
  lib,
  inputs,
  ...
}: let
  cfg = config.twig.programs.river;
in {
  options.twig.programs.river = {
    enable = lib.mkEnableOption "river";
  };

  config = lib.mkIf cfg.enable {
    gtk = let
      gtkWindowBarWorkaround = ''
        headerbar.default-decoration {
          margin-bottom: 50px;
          margin-top: -100px;
        }

        window.csd,
        window.csd decoration {
          box-shadow: none;
        }
      '';
    in {
      gtk3.extraCss = gtkWindowBarWorkaround;
      gtk4.extraCss = gtkWindowBarWorkaround;
    };

    wayland.windowManager.river = {
      enable = true;
      systemd.enable = true;
      settings = {
        rule-add = [
          "-app-id thunar ssd"
          "-app-id firefox ssd"
          "-app-id emacs ssd"
        ];
        focus-follows-cursor = "always";
        attach-mode = "bottom";
        border-width = 2;
        border-color-unfocused = "0x2A2A2D";
        border-color-focused = "0xF8C8DC";
        background-color = "0x0F1010";

        input."*Touchpad" = {
          natural-scroll = "enabled";
          pointer-accel = 0.5;
        };

        map.normal = let
          grimblastCmd = "${lib.getExe pkgs.grimblast} save area -";
          wl-copyCmd = lib.getExe' pkgs.wl-clipboard "wl-copy";
          sattyCmd = "${lib.getExe pkgs.satty} -f -";
          hyprpickerCmd = "${lib.getExe pkgs.hyprpicker} -a -f hex";
          tofiCmd = "${lib.getExe' pkgs.tofi "tofi-drun"} --font ${config.twig.theming.fonts.monospaceFont.package}/share/fonts/truetype/NerdFonts/CaskaydiaCove/CaskaydiaCoveNerdFontMono-Regular.ttf --hint-font false --prompt \"\" --prompt-padding 0";
        in {
          "Super+Shift Return" = "spawn $RIVER_TERMINAL";

          "Super G" = "spawn '${grimblastCmd} | ${wl-copyCmd}'";
          "Super+Shift G" = "spawn '${grimblastCmd} | ${sattyCmd}'";

          "Super P" = "spawn '${hyprpickerCmd}'";

          "Super R" = "spawn '${tofiCmd}'";

          "Super 1" = "set-focused-tags 1";
          "Super 2" = "set-focused-tags 2";
          "Super 3" = "set-focused-tags 4";
          "Super 4" = "set-focused-tags 8";
          "Super Q" = "set-focused-tags 16";
          "Super W" = "set-focused-tags 32";
          "Super E" = "set-focused-tags 64";

          "Control+Shift Q" = "close";
          "Super space" = "toggle-float";
          "Super F" = "toggle-fullscreen";
        };
      };
      extraConfig = let
        wlrCmd = lib.getExe pkgs.wlr-randr;
        autoRotateScript =
          pkgs.writeShellScriptBin "autoRotateScreen"
          ''
            ${lib.getExe' pkgs.iio-sensor-proxy "monitor-sensor"} --accel | while IFS= read -r line; do
              if (echo $line | grep -q "normal"); then
                ${wlrCmd} --output eDP-1 --transform normal;
              elif (echo $line | grep -q "right-up"); then
                ${wlrCmd} --output eDP-1 --transform 90;
              elif (echo $line | grep -q "left-up"); then
                ${wlrCmd} --output eDP-1 --transform 270;
              elif (echo $line | grep -q "bottom-up"); then
                ${wlrCmd} --output eDP-1 --transform 180;
              fi
            done
          '';
        autoRotateCmd = lib.getExe autoRotateScript;
      in ''
        ${autoRotateCmd} &
        ${lib.getExe' inputs.quickshell.packages.${pkgs.system}.quickshell "quickshell"} &
        riverctl default-layout bsp-layout
        ${lib.getExe pkgs.river-bsp-layout} -i 5 -o 10 &
      '';
    };
  };
}
