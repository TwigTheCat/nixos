{...}: {
  home-manager.sharedModules = [
    ./ags
    ./browsers
    ./hypr
    ./niri.nix
    ./idle.nix
    ./cli
    ./editors
    ./gui
    ./music
    ./shells
    ./nix-index.nix
    ./river.nix
    ./foot.nix
    ./fuzzel.nix
    ./wbg.nix
    ./mako.nix
    ./theming.nix
    ./quickshell.nix
    ./prismlauncher.nix
    ./desktop
  ];
}
