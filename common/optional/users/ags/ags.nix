{
  pkgs,
  lib,
  inputs,
  config,
  ...
}: let
  cfg = config.twig.services.ags;
in {
  options.twig.services.ags = {
    enable = lib.mkEnableOption "ags";
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      inputs.self.packages.${pkgs.system}.twig-shell
    ];
  };
}
