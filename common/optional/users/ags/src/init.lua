local astal = require("astal")
local astalgtk3 = require("astal.gtk3")

local App = require("astal.gtk3.app")
local Widget = require("astal.gtk3.widget")
local GLib = astal.require("GLib")
local Anchor = astalgtk3.Astal.WindowAnchor
local Variable = astal.Variable

local function srcPath(path)
  local str = debug.getinfo(2, "S").source:sub(2)
  local src = str:match("(.*/)") or str:match("(.*\\)") or "./"
  return src .. path
end

local scss = srcPath("style.scss")
local css = "/tmp/style.css"

astal.exec("sass " .. scss .. " " .. css)

local function Time(format)
  local time = Variable(""):poll(
    1000,
    function() return GLib.DateTime.new_now_local():format(format) end
  )

  return Widget.Label({
    class_name = "Time",
    on_destroy = function() time:drop() end,
    label = time(),
  })
end

TopBar = function(monitor)
  return Widget.Window({
    monitor = monitor,
    anchor = Anchor.TOP + Anchor.LEFT + Anchor.RIGHT,
    exclusivity = "EXCLUSIVE",
    Widget.Box({
      Time("%I:%M%p %A, %b %e, %Y")
    }),
  })
end

App:start({
  css = css,
  main = function()
    TopBar(0)
  end
})
