{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: let
  cfg = config.twig.programs.prism;
in {
  options.twig.programs.prism = {
    enable = lib.mkEnableOption "prism launcher";
  };

  config = lib.mkIf cfg.enable {
    home.packages = [
      (pkgs.prismlauncher.override {
        jdks = [
          pkgs.temurin-bin-17
        ];
      })
    ];
    home.file.".local/share/PrismLauncher/tmp_prismlauncher.cfg" = {
      text = lib.generators.toINI {} {
        General = {
          ApplicationTheme = "dark";
          BackgroundCat = "teawie";
          IconTheme = "flat_white";
          Language = "en_US";
          MinMemAlloc = "1024";
          MaxMemAlloc = "8192";
          # Performance
          EnableFeralGamemode = "true";
          # Compatibility
          UseNativeGLFW = "true";
          CustomGLFWPath = "${pkgs.glfw}/lib/libglfw.so";
          UseNativeOpenAL = "true";
          # Others
          DownloadsDirWatchRecursive = true;
        };
      };
      onChange = ''
        rm -f ${config.home.homeDirectory}/.local/share/PrismLauncher/prismlauncher.cfg
        cp ${config.home.homeDirectory}/.local/share/PrismLauncher/tmp_prismlauncher.cfg ${config.home.homeDirectory}/.local/share/PrismLauncher/prismlauncher.cfg
        chmod u+w ${config.home.homeDirectory}/.local/share/PrismLauncher/prismlauncher.cfg
      '';
    };
  };
}
