{
  inputs,
  pkgs,
  lib,
  config,
  ...
}: let
  cfg = config.twig.services.minecraft;
in {
  options.twig.services.minecraft = {
    enable = lib.mkEnableOption "minecraft";
  };

  imports = [inputs.nix-minecraft.nixosModules.minecraft-servers];

  config = lib.mkIf cfg.enable {
    nixpkgs.overlays = [inputs.nix-minecraft.overlay];

    services.minecraft-servers = {
      enable = true;
      eula = true;

      servers.twigModpack = let
        modpack = pkgs.fetchPackwizModpack {
          url = "https://git.disroot.org/TwigTheCat/twigModpack/raw/branch/main/pack.toml";
          packHash = "sha256-Yphr4Bxe+geQjUUZz6UiVycTbeyBR7UGVBjT/aHIFzg=";
        };
      in {
        enable = true;

        package = pkgs.fabricServers.fabric-1_20_1.override {loaderVersion = "0.16.9";};

        autoStart = true;
        restart = "no";
        serverProperties = {
          server-port = 4020;

          difficulty = 3;
          gamemode = 0;

          max-players = 1;
          white-list = true;

          motd = "erm, what the sigma?";
        };

        whitelist = {
          ImNotTwig = "8d545d81-7b2d-4bf5-aecf-5dac82e243f6";
        };

        symlinks = {
          "mods" = "${modpack}/mods";
        };

        jvmOpts = ''
          -Xms4096M -Xmx6144M
        '';
      };
    };
  };
}
