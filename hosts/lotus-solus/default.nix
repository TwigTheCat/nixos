{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
  ];

  twig = {
    pamU2fIfWheel.enable = true;

    wireless.enable = true;

    services = {
      snapcast.client.enable = true;
      pipewire.enable = true;
    };

    users = {
      ly.enable = true;
      idiot.enable = true;
    };
  };

  boot.plymouth = {
    enable = true;
    theme = "cuts_alt";
    themePackages = [
      # By default we would install all themes
      (pkgs.adi1090x-plymouth-themes.override {
        selected_themes = ["cuts_alt"];
      })
    ];
  };

  powerManagement.enable = true;
  powerManagement.cpuFreqGovernor = "schedutil";

  virtualisation = {
    containers.enable = true;
    libvirtd.enable = true;
    waydroid.enable = true;
  };

  networking.hostName = "lotus-solus";
  systemd.network.networks = {
    "110-wireless" = {
      matchConfig.Name = "wlan*";
      networkConfig.DNS = config.networking.nameservers ++ config.services.resolved.fallbackDns;
      routes = [{Metric = 2048;}];
      linkConfig.RequiredForOnline = "routable";
    };

    "100-wired" = {
      address = ["192.168.1.42/24"];
      gateway = ["192.168.1.1"];
      matchConfig.Name = "enp*";
      networkConfig.DNS = config.networking.nameservers ++ config.services.resolved.fallbackDns;
      routes = [{Metric = 1024;}];
      linkConfig.RequiredForOnline = false;
    };
  };

  # services.xserver.videoDrivers = ["amdgpu"];

  hardware = {
    xpadneo.enable = true;
    sensor.iio.enable = true;
    bluetooth = {
      enable = true;
      powerOnBoot = true;
    };

    graphics = {
      enable = true;
      enable32Bit = true;
      # amdvlk has issues, so like, dont use it
      # or do, idc
      extraPackages = [
        pkgs.amdvlk
      ];
      extraPackages32 = [
        pkgs.driversi686Linux.amdvlk
      ];
    };
  };
  programs = {
    fish.enable = true;

    noisetorch.enable = true;
    steam = {
      enable = true;
      extraPackages =
        builtins.attrValues
        {
          inherit
            (pkgs)
            gamescope
            gamemode
            ;
        };
    };

    corectrl = {
      enable = true;
      gpuOverclock.enable = true;
    };

    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
      pinentryPackage = pkgs.pinentry-gnome3;
    };

    hyprland = {
      enable = true;
      # package = inputs.hyprland.packages.${pkgs.system}.hyprland;
      # portalPackage = inputs.hyprland.packages.${pkgs.system}.xdg-desktop-portal-hyprland;
      xwayland.enable = true;
    };
  };
  services = {
    displayManager.enable = false;
    blueman.enable = true;
    tailscale.enable = true;

    ratbagd.enable = true;
    logrotate.enable = false;

    avahi = {
      enable = true;
      nssmdns4 = true;
      nssmdns6 = true;
      publish = {
        enable = true;
        addresses = true;
        domain = true;
        hinfo = true;
        userServices = true;
        workstation = true;
      };
    };

    libinput.enable = true;
    kanata = {
      enable = true;
      keyboards."internalKeyboard" = {
        devices = [];
        extraDefCfg = "process-unmapped-keys yes";
        config = ''
          (defsrc s k rctrl ralt)

          (defalias
            switch-to-enter (tap-hold 150 150 spc (layer-while-held enter)))

          (deflayer default
            _ _ @switch-to-enter comp)

          (deflayer enter
            ret bspc _ _)
        '';
      };
    };

    logind.extraConfig = ''
      HandlePowerKey=ignore
    '';

    power-profiles-daemon.enable = false;
    # auto-cpufreq = {
    #   enable = true;
    # };
    tlp = {
      enable = true;
      settings = {
        SOUND_POWER_SAVE_ON_BAT = 1;
        SOUND_POWER_SAVE_ON_AC = 0;

        WIFI_PWR_ON_AC = "on";
        WIFI_PWR_ON_BAT = "on";

        WOL_DISABLE = "Y";

        CPU_SCALING_GOVERNOR_ON_AC = "performance";
        CPU_SCALING_GOVERNOR_ON_BAT = "powersave";

        CPU_ENERGY_PERF_POLICY_ON_AC = "performance";
        CPU_ENERGY_PERF_POLICY_ON_BAT = "power";

        CPU_BOOST_ON_AC = 1;
        CPU_BOOST_ON_BAT = 0;

        USB_AUTOSUSPEND = 0;

        # RUNTIME_PM_BLACKLIST = "05:00.3 05:00.3";
      };
    };

    # envfs.enable = true;
    pcscd.enable = true;
    openssh.enable = true;
    flatpak.enable = true;
    upower.enable = true;
  };

  fonts.fontconfig.subpixel.rgba = "rgb";

  environment = {
    sessionVariables = {
      GST_PLUGIN_SYSTEM_PATH_1_0 = lib.makeSearchPathOutput "lib" "lib/gstreamer-1.0" (builtins.attrValues {
        inherit
          (pkgs.gst_all_1)
          gst-plugins-good
          gst-plugins-bad
          gst-plugins-ugly
          gst-libav
          ;
      });

      NIXOS_OZONE_WL = "1";
      # DOTNET_ROOT = "${pkgs.dotnet-sdk}";
    };

    systemPackages = builtins.attrValues {
      inherit
        (pkgs)
        neovim
        wget
        git
        cryptsetup
        ;
    };
  };

  xdg.portal = {
    config.common.default = "gtk;gnome;hyprland;wlr";
    enable = true;
    wlr.enable = true;
    extraPortals = [
      # inputs.hyprland.packages.${pkgs.system}.xdg-desktop-portal-hyprland
      pkgs.xdg-desktop-portal-gnome
      pkgs.xdg-desktop-portal-hyprland
      pkgs.xdg-desktop-portal-gtk
    ];
  };
  networking.firewall.enable = false;
  system.stateVersion = "24.05";
}
