{
  config,
  pkgs,
  lib,
  modulesPath,
  ...
}: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];
  boot = {
    initrd = {
      availableKernelModules = ["xhci_pci" "ehci_pci" "ahci" "usb_storage" "usbhid" "sd_mod" "sr_mod"];
      kernelModules = [];
    };
    kernelModules = ["kvm-intel"];
    kernelPackages = pkgs.linuxPackages_latest;
    extraModulePackages = [];
    supportedFilesystems = ["btrfs"];
  };
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/root";
      fsType = "xfs";
    };
    "/boot" = {
      device = "/dev/disk/by-label/boot";
      fsType = "vfat";
      options = ["fmask=0077" "dmask=0077"];
    };

    "/srv/media/Downloads" = {
      device = "/dev/disk/by-label/media";
      fsType = "btrfs";
      options = ["compress=zstd" "noatime" "subvol=Downloads"];
    };
    "/srv/media/Pictures" = {
      device = "/dev/disk/by-label/media";
      fsType = "btrfs";
      options = ["compress=zstd" "noatime" "subvol=Pictures"];
    };
    "/srv/media/Music" = {
      device = "/dev/disk/by-label/media";
      fsType = "btrfs";
      options = ["compress=zstd" "noatime" "subvol=Music"];
    };
    "/srv/media/Videos" = {
      device = "/dev/disk/by-label/media";
      fsType = "btrfs";
      options = ["compress=zstd" "noatime" "subvol=Videos"];
    };
    "/srv/media/Documents" = {
      device = "/dev/disk/by-label/media";
      fsType = "btrfs";
      options = ["compress=zstd" "noatime" "subvol=Documents"];
    };
  };

  zramSwap = {
    enable = true;
    priority = 10;
  };
  swapDevices = [
    {
      device = "/var/swapfile";
      priority = 5;
      size = 8192;
    }
  ];

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
