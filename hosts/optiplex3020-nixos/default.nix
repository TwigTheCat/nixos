{
  inputs,
  config,
  pkgs,
  ...
}: {
  imports = [
    ./hardware-configuration.nix
  ];

  twig = {
    users.ly.enable = true;

    services = {
      minecraft.enable = true;
      mpd = {
        enable = true;
        snapserverOutput = true;
        musicDir = "/srv/media/Music";
      };
      snapcast = {
        enable = true;
        mpdLocation = "/run/snapserver/mpd";
      };
    };
  };
  users.users.ly.extraGroups = [
    "minecraft"
    "media"
  ];

  boot.plymouth = {
    enable = true;
    theme = "cuts_alt";
    themePackages = [
      # By default we would install all themes
      (pkgs.adi1090x-plymouth-themes.override {
        selected_themes = ["cuts_alt"];
      })
    ];
  };

  networking.hostName = "optiplex3020-nixos";
  systemd.network.networks."50-enp" = {
    matchConfig.Name = "enp*";
    networkConfig.MulticastDNS = true;
    address = [
      "192.168.1.69/24"
    ];
    gateway = ["192.168.1.1"];
    dns = config.networking.nameservers ++ ["127.0.0.1"];
  };

  networking.firewall.enable = false;

  programs = {
    fish.enable = true;
  };

  services = {
    displayManager.enable = false;
    openssh.enable = true;
    tailscale.enable = true;

    jellyfin = {
      enable = true;
      openFirewall = false;
      user = "ly";
    };

    soft-serve = {
      enable = true;
      package = inputs.self.packages.${pkgs.system}.soft-serve;
      settings = {
        initial_admin_keys = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINhe9wNCdfRsHJLvNil0BwViyJ0Ir/N0Xo0cmkdC8N7h"];
      };
    };
  };

  hardware.graphics = {
    enable = true;
    extraPackages = builtins.attrValues {
      inherit
        (pkgs)
        intel-media-driver
        intel-vaapi-driver
        vaapiVdpau
        libvdpau-va-gl
        ;
    };
  };

  environment.systemPackages =
    builtins.attrValues
    {
      inherit
        (pkgs)
        vim
        git
        wget
        dconf
        # jellyfin stuff
        jellyfin
        jellyfin-web
        jellyfin-ffmpeg
        ;
    };

  system.stateVersion = "24.11";
}
